<img src="/doc/img/sculpture.png" width="260"> <img src="/doc/img/teapot.png" width="260"> <img src="/doc/img/twoboxes.png" width="260">

# Synopsis

Lightmap3ds loads a 3ds model, generates lightmaps to represent its illumination data and renders it in an environment where the user can control the camera. This program leverages the following concepts:

* Octree generation and traversal
* Conservative rasterization
* Lightmap packing
* Multitexturing

The program is currently only supported on the Microsoft Windows operating system.

# Building

1. Open the solution in src\Lightmap3ds.sln with Visual Studio 2015 or better
2. Set the configuration to Release mode for x86
3. Build away!

# Usage

```console
Lightmap3ds.exe <filename> <switches>

<Switches>
-blur: Additionally blurs the generated lightmaps
-dump3ds: Lists the contents of the 3ds file
-dumpbakedmodel: Bakes the 3ds model, lists its contents and writes the lightmap bits to disk
-forcesmoothing: Forces the 3ds model normals to be smoothed
-quality: Set the multiplier for how much lumel space the faces use. The default value is 1
-rendergrid: Renders a reference grid on the y=0 plane
-renderunbaked: Renders the 3ds model using native OpenGL lighting

<Examples>
Lightmap3ds.exe boxes.3ds
Lightmap3ds.exe statue.3ds -dumpbakedmodel
Lightmap3ds.exe small_sphere.3ds -quality 8
Lightmap3ds.exe "D:\complex_model.3ds" -renderunbaked
```

# Controlling the camera

* Hold down Left Mouse Button and move mouse: Orbits the camera around object
* Hold down Right Mouse Button and move mouse: Zooms the camera in and out of the object
* Hold down Middle Mouse Button and move mouse: Pans the camera along the object

# Attributions

The lib3ds, CxImage, OpenGL and GLUT libraries are copyright and licensed by their respective owners.
