#include <lib3ds\light.h>
#include <math.h>
#include "lightmapLight.h"

LightmapLight LightmapLight::From3dsLight(const Lib3dsLight* sourceLight)
{
	LightmapLight light;
	light.type = (sourceLight->spot_light) ? LightmapLight::Spot : LightmapLight::Omni;
	light.position = Vector::From3dsVector(sourceLight->position);
	light.spotDirection = (Vector::From3dsVector(sourceLight->spot) - light.position).Normalized();
	light.color = LightmapColor(sourceLight->color);
	light.hotspotCosine = cosf(sourceLight->hot_spot * 0.5f * 3.14159f / 180.0f);
	light.cutoffCosine = cosf(sourceLight->fall_off * 0.5f * 3.14159f / 180.0f);
	return light;
}
