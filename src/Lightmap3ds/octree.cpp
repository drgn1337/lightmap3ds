#include "octree.h"
#include "octreeFace.h"
#include "minmax.h"
#include "lightmapPatch.h"
#include "lightmapFace.h"
#include "bakedModel.h"
#include "vectorMath.h"
#include <math.h>

Octree::Octree()
{
	parent = nullptr;
	root = nullptr;
	children = nullptr;
	neighborXPos = nullptr;
	neighborXNeg = nullptr;
	neighborYPos = nullptr;
	neighborYNeg = nullptr;
	neighborZPos = nullptr;
	neighborZNeg = nullptr;
	depth = 0;
}

Octree::Octree(Octree* parent, const Box& bounds)
	: Octree()
{
	this->parent = parent;
	this->root = parent->root;
	this->bounds = bounds;
	depth = parent->depth + 1;
}

Octree::~Octree()
{
	DeleteChildren();

	if (IsRoot())
	{
		// The elements in the face list were allocated by the root node,
		// so they should be destroyed as well
		DeleteFaces();
	}
}

void Octree::DeleteChildren()
{
	if (nullptr != children)
	{
		for (int i = 0; i < MaxChildren; i++)
		{
			delete children[i];
		}
	}
	delete children;
	children = nullptr;
}

void Octree::DeleteFaces()
{
	for (auto face : faces)
	{
		delete face;
	}
	faces.clear();
}

bool Octree::CanSubdivide() const
{
	return (faces.size() > 16
		&& fabs(bounds.max.x - bounds.min.x) > 0.01f
		&& fabs(bounds.max.y - bounds.min.y) > 0.01f
		&& fabs(bounds.max.z - bounds.min.z) > 0.01f
		);
}

void Octree::SetNeighbors(Octree* xpos, Octree* xneg, Octree* ypos, Octree* yneg, Octree* zpos, Octree* zneg)
{
	neighborXPos = xpos;
	neighborXNeg = xneg;
	neighborYPos = ypos;
	neighborYNeg = yneg;
	neighborZPos = zpos;
	neighborZNeg = zneg;
}

void Octree::AddFaces(const std::vector<LightmapFace*> lightmapFaces)
{
	for (auto lightmapFace : lightmapFaces)
	{
		AddFace(lightmapFace);
	}
}

void Octree::AddFace(const LightmapFace* lightmapFace)
{
	OctreeFace *octreeFace = OctreeFace::FromLightmapFace(lightmapFace);
	faces.push_back(octreeFace);
}

void Octree::CalculateBoundsFromFaces()
{
	bounds.Zero();

	if (faces.size() > 0)
	{
		bounds = faces.front()->GetBounds();
		for (auto face : faces)
		{
			bounds.Combine(face->GetBounds());
		}
	}
}

void Octree::InheritFacesInsideOrIntersectingBoundsFromParent()
{
	for (size_t face = 0; face < parent->faces.size(); face++)
	{
		OctreeFace::BoxTestResult testResult = parent->faces[face]->BoxTest(bounds);
		switch (testResult) 
		{

		// If the face is completely within boundsToTest then add it to this node and remove it from the source node
		case OctreeFace::BoxTestResult::InBounds:
			faces.push_back(parent->faces[face]);
			parent->faces.erase(parent->faces.begin() + face);
			break;

		// If the face is partially within boundsToTest then add it to this node but leave it in the parent node as well
		// so that it may be shared with other nodes
		case OctreeFace::BoxTestResult::Intersects:
			faces.push_back(parent->faces[face]);
			break;

		// In all other cases ignore the face
		default:
			break;

		}
	}
}

void Octree::Subdivide()
{
	if (CanSubdivide())
	{
		Vector center = GetCenter();
		const Vector& min = bounds.min;
		const Vector& max = bounds.max;

		// Create the children
		Octree** c = new Octree*[MaxChildren];
		c[0] = new Octree(this, Box(min.x, min.y, min.z, center.x, center.y, center.z)); // X- Y- Z-
		c[1] = new Octree(this, Box(min.x, min.y, center.z, center.x, center.y, max.z)); // X- Y- Z+
		c[2] = new Octree(this, Box(min.x, center.y, min.z, center.x, max.y, center.z)); // X- Y+ Z-
		c[3] = new Octree(this, Box(min.x, center.y, center.z, center.x, max.y, max.z)); // X- Y+ Z+
		c[4] = new Octree(this, Box(center.x, min.y, min.z, max.x, center.y, center.z)); // X+ Y- Z-
		c[5] = new Octree(this, Box(center.x, min.y, center.z, max.x, center.y, max.z)); // X+ Y- Z+
		c[6] = new Octree(this, Box(center.x, center.y, min.z, max.x, max.y, center.z)); // X+ Y+ Z-
		c[7] = new Octree(this, Box(center.x, center.y, center.z, max.x, max.y, max.z)); // X+ Y+ Z+

		// Assign the neighbors in a way that's easy to read and hard to mess up. Each
		// dimension is an axe [X][Y][Z] where a higher ordinal mean a more positive direction.
		// Think of the matrix as a 4x4x4 cube containing a 2x2x2 center consisting of this
		// node's children and a surrounding cube that contains extra-child neighbor info.
		Octree* neighborMatrix[4][4][4];
		memset(neighborMatrix, 0, sizeof(neighborMatrix));
		neighborMatrix[1][1][1] = c[0];
		neighborMatrix[1][1][2] = c[1];
		neighborMatrix[1][2][1] = c[2];
		neighborMatrix[1][2][2] = c[3];
		neighborMatrix[2][1][1] = c[4];
		neighborMatrix[2][1][2] = c[5];
		neighborMatrix[2][2][1] = c[6];
		neighborMatrix[2][2][2] = c[7];
		// Now that we've defined neighborly relationships between the children,
		// we need to define them for nodes which bound this one
		// X-
		neighborMatrix[0][1][1] = neighborXNeg;
		neighborMatrix[0][1][2] = neighborXNeg;
		neighborMatrix[0][2][1] = neighborXNeg;
		neighborMatrix[0][2][2] = neighborXNeg;
		// X+
		neighborMatrix[3][1][1] = neighborXPos;
		neighborMatrix[3][1][2] = neighborXPos;
		neighborMatrix[3][2][1] = neighborXPos;
		neighborMatrix[3][2][2] = neighborXPos;
		// Y-
		neighborMatrix[1][0][1] = neighborYNeg;
		neighborMatrix[1][0][2] = neighborYNeg;
		neighborMatrix[2][0][1] = neighborYNeg;
		neighborMatrix[2][0][2] = neighborYNeg;
		// Y+
		neighborMatrix[1][3][1] = neighborYPos;
		neighborMatrix[1][3][2] = neighborYPos;
		neighborMatrix[2][3][1] = neighborYPos;
		neighborMatrix[2][3][2] = neighborYPos;
		// Z-
		neighborMatrix[1][1][0] = neighborZNeg;
		neighborMatrix[1][2][0] = neighborZNeg;
		neighborMatrix[2][1][0] = neighborZNeg;
		neighborMatrix[2][2][0] = neighborZNeg;
		// Z+
		neighborMatrix[1][1][3] = neighborZPos;
		neighborMatrix[1][2][3] = neighborZPos;
		neighborMatrix[2][1][3] = neighborZPos;
		neighborMatrix[2][2][3] = neighborZPos;

		// Now that the neighbor matrix is fully built, percolate that
		// information to the children
		for (int z = 1; z <= 2; z++)
		{
			for (int y = 1; y <= 2; y++)
			{
				for (int x = 1; x <= 2; x++)
				{
					neighborMatrix[x][y][z]->SetNeighbors
					(
						neighborMatrix[x+1][y][z],
						neighborMatrix[x-1][y][z],
						neighborMatrix[x][y+1][z],
						neighborMatrix[x][y-1][z],
						neighborMatrix[x][y][z+1],
						neighborMatrix[x][y][z-1]
					);
				}
			}
		}

		// At this point all the child neighbor information is filled. Note
		// that as we subdivide the nodes, we do not update the neighbors. That
		// means a raycast can go from one small node with a depth of 5 to a 
		// big node with a depth of 1. In a future implementation, the act of
		// subdiving a node should test the depth of its neighbors; and if the
		// neighbor's depth is shallower than this node's depth yet the neighbor
		// is subdivided, then the neighbors should be updated to be more granular
		
		// Now assign the children to this node
		children = c;

		// Distribute faces to the children as possible
		for (int childIndex = 0; childIndex < MaxChildren; childIndex++)
		{
			children[childIndex]->InheritFacesInsideOrIntersectingBoundsFromParent();
		}

		// Subdivide further
		for (int childIndex = 0; childIndex < MaxChildren; childIndex++)
		{
			children[childIndex]->Subdivide();
		}
	}
}

bool Octree::IsRoot() const
{
	return (this == root);
}

bool Octree::IsLeaf() const
{
	return (nullptr == children);
}

Vector Octree::GetCenter() const
{
	Vector center(
		(bounds.min.x + bounds.max.x) * 0.5f,
		(bounds.min.y + bounds.max.y) * 0.5f,
		(bounds.min.z + bounds.max.z) * 0.5f
	);
	return center;
}

Vector Octree::GetSize() const
{
	return bounds.GetSize();
}

Octree** Octree::GetChildren() const
{
	return children;
}

RaycastHit Octree::Raycast(const RaycastSettings& settings) const
{
	const Ray& ray = settings.ray;
	const float infinitesimalAmount = 0.0001f;
	Vector originInsideLeaf = ray.origin;
	RaycastHit noHitResult;
	
	// Find the leaf node containing the origin
	const Octree* leafContainingOrigin = GetLeafContainingPoint(ray.origin);
	if (nullptr == leafContainingOrigin)
	{
		// If the origin is not in a node then do a raycast to see where it would enter the tree
		Vector intersection;
		bool rayIntersectsOctree = GetBoundingBoxIntersectionPoint(ray, intersection);
		if (!rayIntersectsOctree)
		{
			// The ray does not intersect with this octree
			return noHitResult;
		}
		else
		{
			// Move the origin of the ray into the octree
			originInsideLeaf = intersection + ray.direction * infinitesimalAmount;
			// Calculate the leaf containing the origin
			leafContainingOrigin = GetLeafContainingPoint(originInsideLeaf);
		}
	}

	// Traverse through the octree searching for a face to hit
	while (nullptr != leafContainingOrigin)
	{
		// Go through all the leaf faces searching for a hit
		for (OctreeFace* face : leafContainingOrigin->faces)
		{
			if (face->GetSourceFace() != settings.faceToIgnore)
			{
				RaycastHit faceResult = face->Raycast(ray);
				if (faceResult.didHit)
				{
					return faceResult;
				}
			}
		}

		// If we get here, the ray hits none of the faces in this node. Find out where the ray exits this node
		Vector nextIntersection;
		leafContainingOrigin->GetBoundingBoxIntersectionPoint(Ray(originInsideLeaf, ray.direction), nextIntersection);
		// Now find the node immediately in front of that intersection point
		originInsideLeaf = nextIntersection + ray.direction * infinitesimalAmount;
		leafContainingOrigin = GetLeafContainingPoint(originInsideLeaf);
	}

	// The ray exited the octree without hitting any faces
	return noHitResult;
}

const Octree* Octree::GetLeafContainingPoint(const Vector& point) const
{
	if (ContainsPoint(point))
	{
		if (IsLeaf())
		{
			return this;
		}
		else
		{
			for (int i=0; i < MaxChildren; i++)
			{
				const Octree* containingNode = children[i]->GetLeafContainingPoint(point);
				if (nullptr != containingNode)
				{
					return containingNode;
				}
			}
		}
	}
	return nullptr;
}

bool Octree::ContainsPoint(const Vector& point) const
{
	return (point.x >= bounds.min.x && point.x <= bounds.max.x
		&& point.y >= bounds.min.y && point.y <= bounds.max.y
		&& point.z >= bounds.min.z && point.z <= bounds.max.z);
}

bool Octree::GetBoundingBoxIntersectionPoint(const Ray& ray, Vector& intersection) const
{
	float originAxes[3] = { ray.origin.x, ray.origin.y, ray.origin.z };
	float directionAxes[3] = { ray.direction.x, ray.direction.y, ray.direction.z };
	float boxMinAxes[3] = { bounds.min.x, bounds.min.y, bounds.min.z };
	float boxMaxAxes[3] = { bounds.max.x, bounds.max.y, bounds.max.z };
	float tNear, tFar;
	bool nearFarPopulated = false;

	// Perform a ray-box intersection check on each axis
	for (int i = 0; i < 3; i++)
	{
		if (directionAxes[i] == 0)
		{
			// If we get here then the ray is parallel to the axis.
			// Fail if it does not intersect the bounding min max
			if (originAxes[i] < boxMinAxes[i] || originAxes[i] > boxMaxAxes[i])
			{
				return false;
			}
		}
		else
		{
			// Otherwise calculate the intersections of the box planes
			// Calculate the times when the intersections occur
			float t1 = (boxMinAxes[i] - originAxes[i]) / directionAxes[i];
			float t2 = (boxMaxAxes[i] - originAxes[i]) / directionAxes[i];
			if (t1 > t2)
			{
				// Make sure t2 >= t1
				float t = t1;
				t1 = t2; t2 = t;
			}

			if (!nearFarPopulated)
			{
				tNear = t1;
				tFar = t2;
				nearFarPopulated = true;
			}
			else
			{
				if (t1 > tNear) tNear = t1;
				if (t2 < tFar) tFar = t2;
			}

			if (tNear > tFar)
			{
				// If tNear > tFar, box is missed
				return false;
			}
			else if (tFar < 0)
			{
				// If tFar < 0, box is behind ray
				return false;
			}
		}
	}

	if (tNear >= 0)
	{
		intersection = ray.origin + ray.direction * tNear;
	}
	else if (tFar >= 0)
	{
		intersection = ray.origin + ray.direction * tFar;
	}
	else
	{
		return false;
	}
	return true;
}

Octree* Octree::FromLightmapFaces(std::vector<LightmapFace*> lightmapFaces)
{
	Octree* tree = new Octree();
	tree->root = tree;	
	tree->AddFaces(lightmapFaces);
	tree->CalculateBoundsFromFaces();
	tree->Subdivide();
	return tree;
}
