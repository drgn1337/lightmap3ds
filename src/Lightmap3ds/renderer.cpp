#include <lib3ds/file.h>
#include <GL/GLUT.H>
#include <memory>
#include "minmax.h"
#include "lightmapBakerSettings.h"
#include "bakedModel.h"
#include "renderer.h"
#include "rendererSetup.h"
#include "rendererSetupUnbakedModel.h"
#include "rendererSetupBakedModel.h"
#include "renderBakedModel.h"
#include "renderAsUnbakedModel.h"
#include "glutSetup.h"
#include "vectorMath.h"
#include "commandLineTools.h"
#include "debugTools.h"
#include "maxpath.h"

// True if the render should be modeled using native OpenGL lighting
bool render_model_as_unbaked = false;

// True if we should render a grid
bool render_grid = false;

// Camera coords
float cameraEye[3] = { 0, 50, 200 };
float cameraCenter[3] = { 0, 0, 0 };
float cameraUp[3] = { 0, 1, 0 };

// The camera coords when any mouse button was most recently pressed
float oldCameraEye[3];
float oldCameraCenter[3];
float oldCameraUp[3];

// Mouse state
bool wasLeftMouseButtonDown = false;
bool wasRightMouseButtonDown = false;
bool wasMiddleMouseButtonDown = false;
int clickX = 0;
int clickY = 0;

// Model data
Lib3dsFile* unbakedModel = nullptr;
BakedModel* bakedModel = nullptr;
char szLib3dModelFilename[MAX_PATH];

bool mouseButtonStateChanged()
{
	return (wasLeftMouseButtonDown != isLeftMouseButtonDown())
		|| (wasRightMouseButtonDown != isRightMouseButtonDown())
		|| (wasMiddleMouseButtonDown != isMiddleMouseButtonDown());
}

void saveMouseButtonState()
{
	wasLeftMouseButtonDown = isLeftMouseButtonDown();
	wasRightMouseButtonDown = isRightMouseButtonDown();
	wasMiddleMouseButtonDown = isMiddleMouseButtonDown();
	clickX = getMouseX();
	clickY = getMouseY();
}

void resetOldCameraState() 
{
	memcpy(oldCameraEye, cameraEye, sizeof(cameraEye));
	memcpy(oldCameraCenter, cameraCenter, sizeof(cameraCenter));
	memcpy(oldCameraUp, cameraUp, sizeof(cameraUp));
}

void orbit()
{
	float fRelX = (clickX - getMouseX()) / (float)getWindowWidth();
	float fRelY = (clickY - getMouseY()) / (float)getWindowHeight();
	float v[3], vRes[3], vRight[3];
	float vv[3] = { 0,1,0 };
	float m[16];

	v[0] = oldCameraEye[0] - oldCameraCenter[0];
	v[1] = oldCameraEye[1] - oldCameraCenter[1];
	v[2] = oldCameraEye[2] - oldCameraCenter[2];

	buildRotationMatrix(vv, fRelX, m);
	transformVector(v, vRes, m);

	vRight[0] = (oldCameraEye[1] - oldCameraCenter[1])*oldCameraUp[2] - (oldCameraEye[2] - oldCameraCenter[2])*oldCameraUp[1];
	vRight[1] = (oldCameraEye[2] - oldCameraCenter[2])*oldCameraUp[0] - (oldCameraEye[0] - oldCameraCenter[0])*oldCameraUp[2];
	vRight[2] = (oldCameraEye[0] - oldCameraCenter[0])*oldCameraUp[1] - (oldCameraEye[1] - oldCameraCenter[1])*oldCameraUp[0];

	float d = vRight[0] * vRight[0] + vRight[1] * vRight[1] + vRight[2] * vRight[2];
	d = -sqrt(d);
	if (d)
	{
		vRight[0] /= d;
		vRight[1] /= d;
		vRight[2] /= d;
	}

	buildRotationMatrix(vRight, fRelY, m);
	transformVector(vRes, v, m);

	cameraEye[0] = cameraCenter[0] + v[0];
	cameraEye[1] = cameraCenter[1] + v[1];
	cameraEye[2] = cameraCenter[2] + v[2];
}

void zoom()
{
	float fRelY = (clickY - getMouseY()) / (float)getWindowHeight();
	float f[3];

	f[0] = oldCameraEye[0] - oldCameraCenter[0];
	f[1] = oldCameraEye[1] - oldCameraCenter[1];
	f[2] = oldCameraEye[2] - oldCameraCenter[2];

	float d = f[0] * f[0] + f[1] * f[1] + f[2] * f[2];
	d = -sqrt(d);
	if (d)
	{
		f[0] /= d;
		f[1] /= d;
		f[2] /= d;
	}

	cameraEye[0] = oldCameraEye[0] + (oldCameraCenter[0] - oldCameraEye[0]) * fRelY;
	cameraEye[1] = oldCameraEye[1] + (oldCameraCenter[1] - oldCameraEye[1]) * fRelY;
	cameraEye[2] = oldCameraEye[2] + (oldCameraCenter[2] - oldCameraEye[2]) * fRelY;
}

void pan()
{
	float fRelX = (clickX - getMouseX()) / (float)getWindowWidth();
	float fRelY = (clickY - getMouseY()) / (float)getWindowHeight();
	float vRight[3];

	vRight[0] = (oldCameraEye[1] - oldCameraCenter[1])*oldCameraUp[2] - (oldCameraEye[2] - oldCameraCenter[2])*oldCameraUp[1];
	vRight[1] = (oldCameraEye[2] - oldCameraCenter[2])*oldCameraUp[0] - (oldCameraEye[0] - oldCameraCenter[0])*oldCameraUp[2];
	vRight[2] = (oldCameraEye[0] - oldCameraCenter[0])*oldCameraUp[1] - (oldCameraEye[1] - oldCameraCenter[1])*oldCameraUp[0];

	float d = vRight[0] * vRight[0] + vRight[1] * vRight[1] + vRight[2] * vRight[2];
	d = -sqrt(d);
	if (d)
	{
		vRight[0] /= d;
		vRight[1] /= d;
		vRight[2] /= d;
	}

	cameraEye[0] = oldCameraEye[0] + vRight[0] * fRelX * 40 - oldCameraUp[0] * fRelY * 40;
	cameraEye[1] = oldCameraEye[1] + vRight[1] * fRelX * 40 - oldCameraUp[1] * fRelY * 40;
	cameraEye[2] = oldCameraEye[2] + vRight[2] * fRelX * 40 - oldCameraUp[2] * fRelY * 40;
	cameraCenter[0] = oldCameraCenter[0] + vRight[0] * fRelX * 40 - oldCameraUp[0] * fRelY * 40;
	cameraCenter[1] = oldCameraCenter[1] + vRight[1] * fRelX * 40 - oldCameraUp[1] * fRelY * 40;
	cameraCenter[2] = oldCameraCenter[2] + vRight[2] * fRelX * 40 - oldCameraUp[2] * fRelY * 40;
}

void positionCamera() 
{
	if (mouseButtonStateChanged())
	{
		resetOldCameraState();
		saveMouseButtonState();
	}

	if (isLeftMouseButtonDown())
	{
		orbit();
	}
	else if (isRightMouseButtonDown())
	{
		zoom();
	}
	else if (isMiddleMouseButtonDown())
	{
		pan();
	}
}

void lookThroughCamera()
{
	glLoadIdentity();
	gluLookAt(cameraEye[0], cameraEye[1], cameraEye[2], cameraCenter[0], cameraCenter[1], cameraCenter[2],
		cameraUp[0], cameraUp[1], cameraUp[2]);
}

void renderGrid()
{
	glPushAttrib(GL_LIGHTING_BIT);
	glDisable(GL_LIGHTING);
	glBegin(GL_LINES);
	glColor3f(0.2f, 0.2f, 0.2f);
	for (GLfloat z = -200.0f; z <= 200.0f; z += 20.0f)
	{
		if (z >= -0.1f && z <= 0.1f) glColor3f(0.5f, 0.5f, 0.5f);
		glVertex3f(-200, 0, z);
		glVertex3f(200, 0, z);
		if (z >= -0.1f && z <= 0.1f) glColor3f(0.2f, 0.2f, 0.2f);
	}
	for (GLfloat x = -200.0f; x <= 200.0f; x += 20.0f)
	{
		if (x >= -0.1f && x <= 0.1f) glColor3f(0.5f, 0.5f, 0.5f);
		glVertex3f(x, 0, -200);
		glVertex3f(x, 0, 200);
		if (x >= -0.1f && x <= 0.1f) glColor3f(0.2f, 0.2f, 0.2f);
	}
	glEnd();
	glPopAttrib();
}

void renderScene()
{
	positionCamera();
	lookThroughCamera();

	if (render_grid)
	{
		renderGrid();
	}

	if (render_model_as_unbaked)
	{
		renderAsUnbakedModel(unbakedModel);
	}
	else
	{
		renderBakedModel(bakedModel);
	}
}

void rendererInit(int argc, char *argv[])
{
	// Read configuration values relevant to the renderer
	if (containsArgument(argc, argv, "-renderunbaked"))
	{
		render_model_as_unbaked = true;
	}
	if (containsArgument(argc, argv, "-rendergrid"))
	{
		render_grid = true;
	}

	// Initialize the video engine
	initializeGlutSimulation(argc, argv);

	// Load the model
	strcpy_s(szLib3dModelFilename, MAX_PATH, argv[1]);
	DebugTools::Log("Grocery shopping (loading the model)...\n");
	unbakedModel = load3dsModel(argv[1]);

	// Load the model textures from the path of the 3ds file into the video card
	DebugTools::Log("Unpacking groceries (loading model textures)...\n");
	load3dsModelTexturesIntoGl(argv[1], unbakedModel);

	// Build the glLists that are called in the render loop
	if (render_model_as_unbaked)
	{
		generateGlListsFrom3dsModel(unbakedModel);
	}
	else
	{
		bakedModel = bake3dsModel(unbakedModel, LightmapBakerSettings::SettingsFromCommandLine(argc, argv));
		generateGlLightmapTexturesFromBakedModel(bakedModel);
		generateGlListsFromBakedModel(bakedModel);
	}
}

void rendererMainLoop()
{
	glutMainLoop();
}

void rendererCleanup()
{
	if (nullptr != bakedModel)
	{
		delete bakedModel;
		bakedModel = nullptr;
	}
	else if (nullptr != unbakedModel)
	{
		lib3ds_file_free(unbakedModel);
		unbakedModel = nullptr;
	}
}
