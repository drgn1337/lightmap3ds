#pragma once

// Contains declarations for setup functions related to rendering used by both
// baked and unbaked rendering

struct Lib3dsFile;
class BakedModel;

Lib3dsFile* load3dsModel(const char* szFilename);
void load3dsModelTexturesIntoGl(const char* szFilename, Lib3dsFile* model);
