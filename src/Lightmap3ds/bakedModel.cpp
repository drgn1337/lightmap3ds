#include <lib3ds\file.h>
#include "lightmapPatch.h"
#include "bakedModel.h"
#include "minmax.h"

BakedModel::BakedModel(Lib3dsFile* sourceFile, std::vector<LightmapMaterial*> lightmapMaterials, std::vector<LightmapPatch*> lightmapPatches)
{
	this->sourceFile = sourceFile;
	this->lightmapMaterials = lightmapMaterials;
	this->lightmapPatches = lightmapPatches;
	this->userData = nullptr;
}

BakedModel::~BakedModel()
{
	for (auto lightmapPatch : lightmapPatches)
	{
		LightmapPatch::Destroy(lightmapPatch);
	}
	for (auto lightmapMaterial : lightmapMaterials)
	{
		delete lightmapMaterial;
	}
	lib3ds_file_free(sourceFile);
}

std::vector<LightmapPatch*>& BakedModel::GetLightmapPatches()
{
	return lightmapPatches;
}

void BakedModel::SetUserData(void* p)
{
	userData = p;
}

void* BakedModel::GetUserData() const
{
	return userData;
}
