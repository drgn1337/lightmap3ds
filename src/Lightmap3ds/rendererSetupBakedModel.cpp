#include <lib3ds\mesh.h>
#include <GL\GLUT.H>
#include <GL\wglext.h>
#include "lightmapVertex.h"
#include "lightmapFace.h"
#include "lightmapPatch.h"
#include "lightmapBaker.h"
#include "lightmapMaterial.h"
#include "bakedModel.h"
#include "rendererSetupBakedModel.h"
#include "debugTools.h"

bool didTryLoadMultiTexturingExtensions = false;
bool supportsMultiTexturing = false;

PFNGLACTIVETEXTUREPROC glActiveTexture;
PFNGLMULTITEXCOORD2FPROC glMultiTexCoord2f;

void loadGLMultiTexturingExtensions()
{
	glActiveTexture = (PFNGLACTIVETEXTUREPROC)wglGetProcAddress("glActiveTexture");
	glMultiTexCoord2f = (PFNGLMULTITEXCOORD2FPROC)wglGetProcAddress("glMultiTexCoord2f");
	if (nullptr == glActiveTexture || nullptr == glMultiTexCoord2f)
	{
		DebugTools::LogError("Multitexturing is not supported on this video card. Model textures will not be rendered!");
		supportsMultiTexturing = false;
	}
	else
	{
		supportsMultiTexturing = true;
		glActiveTexture(GL_TEXTURE1);
		glEnable(GL_TEXTURE_2D);
		glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
		glActiveTexture(GL_TEXTURE0);
	}

	didTryLoadMultiTexturingExtensions = true;
}

BakedModel* bake3dsModel(Lib3dsFile *model, const LightmapBakerSettings& settings)
{
	return LightmapBaker::Bake(model, settings);
}

void generateGlLightmapTexturesFromBakedPatch(LightmapPatch *patch)
{
	GLuint textureId;
	glGenTextures(1, &textureId);
	glBindTexture(GL_TEXTURE_2D, textureId);
	glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, 3, LightmapPatch::TextureWidth, LightmapPatch::TextureHeight, 0, GL_BGR_EXT, GL_UNSIGNED_BYTE, patch->GetLumels());
	gluBuild2DMipmaps(GL_TEXTURE_2D, 3, LightmapPatch::TextureWidth, LightmapPatch::TextureHeight, GL_BGR_EXT, GL_UNSIGNED_BYTE, patch->GetLumels());
	patch->SetUserData((void*)textureId);
}

void generateGlLightmapTexturesFromBakedModel(BakedModel* model)
{
	for (auto patch : model->GetLightmapPatches())
	{
		generateGlLightmapTexturesFromBakedPatch(patch);
	}
}

void generateGlListsFromBakedFace(LightmapFace* face)
{
	bool useMultiTexturing = supportsMultiTexturing && nullptr != face->GetSourceMaterial() && 0 != face->GetSourceMaterial()->textureHandle;
	if (useMultiTexturing)
	{
		glActiveTexture(GL_TEXTURE1);
		glEnable(GL_TEXTURE_2D);
		glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
		glBindTexture(GL_TEXTURE_2D, face->GetSourceMaterial()->textureHandle);
		glActiveTexture(GL_TEXTURE0);
	}
	else
	{
		if (supportsMultiTexturing)
		{
			glActiveTexture(GL_TEXTURE1);
			glDisable(GL_TEXTURE_2D);
			glActiveTexture(GL_TEXTURE0);
		}
	}
	
	glBegin(GL_TRIANGLES);
	const LightmapVertex *v = face->GetVertices();
	for (int i = 0; i < LightmapFace::VertexCount; i++, v++)
	{
		if (!supportsMultiTexturing)
		{
			glTexCoord2f(v->lumelPos.u, v->lumelPos.v);
		}		
		else
		{
			glMultiTexCoord2f(GL_TEXTURE0, v->lumelPos.u, v->lumelPos.v);
			glMultiTexCoord2f(GL_TEXTURE1, v->texelPos.u, v->texelPos.v);
		}
		glNormal3f(v->normal.x, v->normal.y, v->normal.z);
		glVertex3f(v->worldPos.x, v->worldPos.y, v->worldPos.z);
	}
	glEnd();
}

void generateGlListsFromBakedPatch(LightmapPatch* patch)
{
	auto face = patch->GetManagedFace();
	if (nullptr != face)
	{
		generateGlListsFromBakedFace(face);
	}

	for (int i = 0; i < LightmapPatch::ChildCount; i++)
	{
		auto child = patch->GetChild(i);
		if (nullptr != child)
		{
			generateGlListsFromBakedPatch(child);
		}
	}
}

void generateGlListsFromBakedModel(BakedModel* model)
{
	if (!didTryLoadMultiTexturingExtensions)
	{
		loadGLMultiTexturingExtensions();
	}

	model->SetUserData((void*)glGenLists(1));
	glNewList((GLuint)model->GetUserData(), GL_COMPILE);
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_CULL_FACE);
	glColor3f(1, 1, 1);
	for (auto patch : model->GetLightmapPatches())
	{
		glBindTexture(GL_TEXTURE_2D, (GLuint)patch->GetUserData());
		generateGlListsFromBakedPatch(patch);
	}
	glDisable(GL_TEXTURE_2D);
	glEndList();
}
