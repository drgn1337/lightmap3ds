#pragma once

#include <vector>
#include "texel.h"
#include "lightmapColor.h"

struct Lib3dsLight;
struct LightmapLight;

class LightmapPatch;
class LightmapFace;
class Octree;

// Responsible for writing lumels to the lightmap texture. Unlike LightmapBaker which works
// at the model level, this object works at the lightmap patch level.
class LightmapLumelBuilder
{
private:
	// The domain of parallelpipeds which encapsulate the model
	Octree* octree;

	// The collection of lights in the scene
	std::vector<LightmapLight> lights;

private:
	// Face world coordinates projected on the favored axis
	float x0, x1, x2;
	float y0, y1, y2;

	// Face UV coordinates
	float u0, v0;
	float u1, v1;
	float u2, v2;
	
	// Intermediate values
	float denominator;
	float dpdu;
	float dpdv;
	float dqdu;
	float dqdv;

public:
	LightmapLumelBuilder(Octree *sourceOctree, std::vector<LightmapLight> sourceLights);

public:
	// Calculates and writes lumels to the patch
	void WriteLumelsToPatch(const LightmapPatch* patch);
private:
	// Updated intermediate values used in calculations for a specific face so we don't have to
	// recalculate them over and over per lumel
	void UpdateIntermediateCalculationVariables(const LightmapFace* face);
	// Calculates and writes lumels to a single lumel of the patch
	void WriteLumelToPatch(const LightmapPatch* patch, int textureX, int textureY);
	// Gets the world position from a lumel position
	Vector GetLumelWorldPos(const LightmapFace* face, const Texel& lumel);
	// Gets the world normal from a lumel position
	Vector GetLumelWorldNorm(const LightmapFace* face, const Vector& worldPos);
	// Gets the color for a single lumel of the patch for all lights combined
	LightmapColor GetLumelColor(const LightmapFace* face, int textureX, int textureY);
	// Gets the color for a single lumel of the patch for a single light
	LightmapColor GetLumelColorForLight(const LightmapLight& light, const LightmapFace* face, const Vector& pos, const Vector& norm);
};
