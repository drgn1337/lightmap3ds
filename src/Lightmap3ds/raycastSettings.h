#pragma once

#include "ray.h"

class LightmapFace;

// Contains the parameters for a raycast
struct RaycastSettings
{
	// The start of the ray and its direction in world space
	Ray ray;
	// The face to ignore in the raycast
	const LightmapFace *faceToIgnore;

	RaycastSettings()
	{
	}

	RaycastSettings(const Ray& ray, const LightmapFace* faceToIgnore)
	{
		this->ray = ray;
		this->faceToIgnore = faceToIgnore;
	}
};
