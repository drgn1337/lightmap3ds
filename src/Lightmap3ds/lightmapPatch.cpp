#include <lib3ds\mesh.h>
#include <math.h>
#include "lightmapFace.h"
#include "lightmapPatch.h"
#include "debugTools.h"

LightmapPatch::LightmapPatch(int x, int y, int w, int h, float quality)
{
	originX = x;
	originY = y;
	width = w;
	height = h;
	managedFace = nullptr;
	root = nullptr;
	child[0] = child[1] = nullptr;
	lumels = nullptr;
	userData = nullptr;
	this->quality = quality;
}

LightmapPatch::~LightmapPatch()
{
	if (IsRootPatch())
	{
		delete[] lumels;
	}
	for (int i = 0; i < 2; i++)
	{
		if (nullptr != child[i])
		{
			delete child[i];
		}
	}
}

LightmapPatch::AddFaceResult LightmapPatch::TrySetFace(LightmapFace* face)
{
	if (IsLeafPatch())
	{
		if (nullptr != managedFace)
		{
			// This is a patch with no children and a face assigned to it. This means the 
			// entire patch is taken up and no more faces can fit into it
			return AddFaceResult::FaceTooBig;
		}
		else
		{
			// If this node has no children then try to store the face in it and
			// create child nodes to occupy the remaining available space of this node
			int faceWidth = GetLightmapFaceTextureWidth(face);
			int faceHeight = GetLightmapFaceTextureHeight(face);
			int textureWidth = GetWidth();
			int textureHeight = GetHeight();

			if (textureWidth < faceWidth || textureHeight < faceHeight)
			{
				// Face is too big for this patch

				return AddFaceResult::FaceTooBig;
			}
			else
			{
				// Face will fit in this patch

				int remainingWidth = textureWidth - faceWidth;
				int remainingHeight = textureHeight - faceHeight;
				int childOriginX[2];
				int childOriginY[2];
				int childWidth[2];
				int childHeight[2];

				if (remainingWidth > remainingHeight)
				{
					// There's more room on the U axis for new faces so
					// make children like so:
					//
					// +----------+----------------+
					// |          |                |
					// |  this    |                |
					// |          |                |
					// |          |    child[0]    |
					// |          |                |
					// +----------+                |
					// | child[1] |                |
					// +----------+----------------+
					//
					childOriginX[0] = originX + faceWidth;
					childOriginY[0] = originY;
					childWidth[0] = textureWidth - faceWidth;
					childHeight[0] = height;

					childOriginX[1] = originX;
					childOriginY[1] = originY + faceHeight;
					childWidth[1] = faceWidth;
					childHeight[1] = height - faceHeight;
				}
				else
				{
					// There's at least as much room on the V axis for 
					// new faces so make children like so:
					//
					// +------------+----------+
					// |    this    | child[0] |
					// +------------+----------+
					// |                       |
					// |                       |
					// |        child[1]       |
					// |                       |
					// |                       |
					// +-----------------------+
					//
					childOriginX[0] = originX + faceWidth;
					childOriginY[0] = originY;
					childWidth[0] = textureWidth - faceWidth;
					childHeight[0] = faceHeight;

					childOriginX[1] = originX;
					childOriginY[1] = originY + faceHeight;
					childWidth[1] = textureWidth;
					childHeight[1] = height - faceHeight;
				}

				// Create empty child patches
				for (int i = 0; i < 2; i++)
				{
					if (childWidth[i] > 0 && childHeight[i] > 0)
					{
						child[i] = new LightmapPatch(childOriginX[i], childOriginY[i], childWidth[i], childHeight[i], quality);
						child[i]->root = root;
					}
				}

				// Calculate the texture UV's of each face vertex
				face->BuildLightmapUVs(originX + LightmapPatch::Padding, originY + LightmapPatch::Padding, quality);
				face->BuildLightmapEdgePlanes();

				// Assign the face to this patch and adjust our patch width and height to fit the face
				managedFace = face;

				return AddFaceResult::Ok;
			}
		}
	}
	else
	{
		// If this patch already has children then this patch already contains a face and
		// we should try to store one in a child patch

		if (nullptr != child[0] && AddFaceResult::Ok == child[0]->TrySetFace(face))
		{
			return AddFaceResult::Ok;
		}
		else if (nullptr != child[1] && AddFaceResult::Ok == child[1]->TrySetFace(face))
		{
			return AddFaceResult::Ok;
		}
		else
		{
			return AddFaceResult::FaceTooBig;
		}
	}
}

void LightmapPatch::Blur()
{
	if (IsRootPatch())
	{
		int bytesPerRow = width * BytesPerLumel;
		int lumelMapSizeInBytes = bytesPerRow * height;
		int ox = GetOriginX();
		int oy = GetOriginY();
		unsigned char* blurredLumels = new unsigned char[lumelMapSizeInBytes];

		for (int y = oy; y < oy + height; y++)
		{
			unsigned char* src = lumels + (y * width + ox) * BytesPerLumel;
			unsigned char* dst = blurredLumels + (y * width + ox) * BytesPerLumel;
			for (int x = ox; x < ox + width; x++, src += BytesPerLumel, dst += BytesPerLumel)
			{
				// Get pointers to the eight neighboring pixels
				unsigned char* p[] =
				{
					(x > ox && y > oy) ? (src - bytesPerRow - BytesPerLumel) : nullptr,
					(y > oy) ? (src - bytesPerRow) : nullptr,
					(x < ox + width - 1 && y > oy) ? (src - bytesPerRow + BytesPerLumel) : nullptr,

					(x > ox) ? (src - BytesPerLumel) : nullptr,
					(x < ox + width - 1) ? (src + BytesPerLumel) : nullptr,

					(x > ox && y < oy + height - 1) ? (src + bytesPerRow - BytesPerLumel) : nullptr,
					(y < oy + height - 1) ? (src + bytesPerRow) : nullptr,
					(x < ox + width - 1 && y < oy + height - 1) ? (src + bytesPerRow + BytesPerLumel) : nullptr,
				};

				// Sum the neighboring pixels and divide by eight
				unsigned long red = 0, green = 0, blue = 0;
				int nComponents = 0;
				for (int i = 0; i < 8; i++)
				{
					if (nullptr != p[i])
					{
						blue += *(p[i]);
						green += *(p[i] + 1);
						red += *(p[i] + 2);
						nComponents++;
					}
				}
				if (nComponents > 0)
				{
					dst[0] = (unsigned char)(blue / nComponents);
					dst[1] = (unsigned char)(green / nComponents);
					dst[2] = (unsigned char)(red / nComponents);
				}
			}
		}

		delete[] lumels;
		lumels = blurredLumels;
	}
}

bool LightmapPatch::IsRootPatch() const
{
	return (this == root);
}

bool LightmapPatch::IsLeafPatch() const
{
	return (nullptr == child[0] && nullptr == child[1]);
}

int LightmapPatch::GetOriginX() const
{
	return originX;
}

int LightmapPatch::GetOriginY() const
{
	return originY;
}

int LightmapPatch::GetWidth() const
{
	return width;
}

int LightmapPatch::GetHeight() const
{
	return height;
}

LightmapFace* LightmapPatch::GetManagedFace() const
{
	return managedFace;
}

unsigned char* LightmapPatch::GetLumels() const
{
	return root->lumels;
}

LightmapPatch* LightmapPatch::GetChild(int childIndex) const
{
	return child[childIndex];
}

void LightmapPatch::SetUserData(void* data)
{
	userData = data;
}

void* LightmapPatch::GetUserData() const
{
	return userData;
}

int LightmapPatch::GetLightmapFaceTextureWidth(const LightmapFace* face) const
{
	return (int)ceilf(face->GetProjectionWidth() * quality) + Padding * 2;
}

int LightmapPatch::GetLightmapFaceTextureHeight(const LightmapFace* face) const
{
	return (int)ceilf(face->GetProjectionHeight() * quality) + Padding * 2;
}

LightmapPatch* LightmapPatch::Create(float quality)
{
	LightmapPatch* patch = new LightmapPatch(0, 0, LightmapPatch::TextureWidth, LightmapPatch::TextureHeight, quality);
	int lumelMapSizeInBytes = patch->width * patch->height * BytesPerLumel;
	patch->lumels = new unsigned char[lumelMapSizeInBytes];
	memset(patch->lumels, 0, lumelMapSizeInBytes);
	patch->root = patch;
	return patch;
}

void LightmapPatch::Destroy(LightmapPatch* patch)
{
	delete patch;
}
