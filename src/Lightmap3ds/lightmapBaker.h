#pragma once

#include <vector>
#include <map>

struct Lib3dsFile;
struct Lib3dsNode;

struct LightmapMaterial;
struct LightmapBakerSettings;

class BakedModel;
class Octree;
class LightmapFace;
class LightmapPatch;

// Contains all the high level functions for baking lightmaps. The actual lumel
// writing is done with the LightmapLumelBuilder object; but everything leading
// up to that including packing the faces into lightmap textures and octree
// construction are managed by this object
class LightmapBaker
{
private:
	// Bake settings
	const LightmapBakerSettings& settings;

	// The model to bake
	Lib3dsFile* fileToBake;

	// The domain of parallelpipeds which encapsulate the model
	Octree* octree;

	// The collection of lightmap materials
	std::map<Lib3dsMaterial*, LightmapMaterial*> materials;

	// The collection of faces which contain a 3ds face and information
	// needed to apply a lightmap texture toe the face
	std::vector<LightmapFace*> faces;

	// The collection of lightmap textures which contain references to
	// lightmap faces and lumel maps for them
	std::vector<LightmapPatch*> lightmapPatches;

private:
	LightmapBaker(Lib3dsFile *fileToBake, const LightmapBakerSettings& settings);
	~LightmapBaker();

private:
	// Loads 3ds material info information into the lightmap builder
	void BuildLightmapMaterials();

	// Build the collection of lightmap faces which we will use for baking
	// and updating the model faces with
	void BuildLightmapFaces();
	void BuildLightmapFacesFrom3dsNode(Lib3dsNode* sourceNode);
	void BuildLightmapFaceFrom3dsFace(Lib3dsNode* sourceNode, Lib3dsMesh* sourceMesh, Lib3dsVector *sourceMeshNormals, Lib3dsFace* sourceFace);

	// Sort the lightmap face collection from largest to smallest projection area
	// for easy size-based traversal and efficient lightmap texture packing
	void SortLightmapFacesByProjectionArea();

	// Generate a collection of blank lightmap textures and patches to house each
	// lightmap face along with information on how to generate lumels for the faces
	void InitializeLightmaps();

	// Create a domain of boxes which encapsulate the model so that
	// we can peform rapid raycasts when we build the light map
	void BuildOctree();

	// Populate the lightmaps with lumel data
	void WriteLumelsToLightmaps();

	// Blur the lightmaps (optional)
	void BlurLightmaps();

public:
	// Bakes a 3ds model and returns an object with everything required to render the baked model
	static BakedModel* Bake(Lib3dsFile *modelToBake, const LightmapBakerSettings& settings);
};
