#pragma once

#include "vector.h"

class LightmapFace;

// A structure containing information returned from raycast calls
struct RaycastHit
{
	// True if the ray hit the object
	bool didHit;
	// The point where the ray hit the object
	Vector point;
	// The distance between the ray's origin and the hit point
	float distance;

	RaycastHit()
	{
		didHit = false;
		point = Vector(0, 0, 0);
		distance = 0;
	}
};
