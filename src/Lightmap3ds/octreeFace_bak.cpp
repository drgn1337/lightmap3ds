#include <lib3ds\file.h>
#include <lib3ds\node.h>
#include <lib3ds\mesh.h>
#include <lib3ds\matrix.h>
#include <lib3ds\vector.h>
#include "octreeFace.h"
#include "minmax.h"

OctreeFace::OctreeFace()
{
	sourceFace = nullptr;
	sourceNode = nullptr;
}

Lib3dsFace* OctreeFace::GetSourceFace() const
{
	return sourceFace;
}

const Box& OctreeFace::GetBounds() const
{
	return bounds;
}

OctreeFace::BoxTestResult OctreeFace::BoxTest(const Box& box) const
{
	// First we do a simple test: If the range of any of the axes in box are completely
	// beyond the range of that of our bounding box, then they must not intersect in any way
	if (bounds.min.x > box.max.x || bounds.max.x < box.min.x ||
		bounds.min.y > box.max.y || bounds.max.y < box.min.y ||
		bounds.min.z > box.max.z || bounds.max.z < box.min.z)
	{
		return OutOfBounds;
	}

	// Next test: Check if box is completely inside our bounding box
	if (bounds.min.x >= box.min.x && bounds.max.x <= box.max.x &&
		bounds.min.y >= box.min.y && bounds.max.y <= box.max.y &&
		bounds.min.z >= box.min.z && bounds.max.z <= box.max.z)
	{
		return InBounds;
	}

	// If we get here, we know that the box is not completely out of range of
	// the bounding box (test 1), but we also know it's not fully contained
	// therein (test 2). Therefore, it must intersect in some way.
	return Intersects;
}

OctreeFace* OctreeFace::From3dsFace(Lib3dsNode* node, Lib3dsMesh* mesh, Lib3dsFace *face)
{
	OctreeFace *octreeFace = new OctreeFace();
	octreeFace->sourceFace = face;
	octreeFace->sourceNode = node;

	Vector firstPoint = From3dsVector(node, mesh, mesh->pointL[face->points[0]].pos);
	octreeFace->bounds.min = octreeFace->bounds.max = firstPoint;

	const int indices = 3;
	for (int i = 1; i < indices; i++)
	{
		Vector point = From3dsVector(node, mesh, mesh->pointL[face->points[i]].pos);
		octreeFace->bounds.min.x = min(octreeFace->bounds.min.x, point.x);
		octreeFace->bounds.min.y = min(octreeFace->bounds.min.y, point.y);
		octreeFace->bounds.min.z = min(octreeFace->bounds.min.z, point.z);
		octreeFace->bounds.max.x = max(octreeFace->bounds.max.x, point.x);
		octreeFace->bounds.max.y = max(octreeFace->bounds.max.y, point.y);
		octreeFace->bounds.max.z = max(octreeFace->bounds.max.z, point.z);
	}
	
	return octreeFace;
}

Vector OctreeFace::From3dsVector(Lib3dsNode* node, Lib3dsMesh* mesh, Lib3dsVector vector)
{
	// Transform the raw 3ds point into world space
	Lib3dsMatrix inv_matrix, M;
	Lib3dsVector worldPos;

	lib3ds_matrix_copy(inv_matrix, mesh->matrix);
	lib3ds_matrix_inv(inv_matrix);
	lib3ds_matrix_copy(M, node->matrix);
	lib3ds_matrix_translate_xyz(M, -node->data.object.pivot[0], -node->data.object.pivot[1], -node->data.object.pivot[2]);
	lib3ds_matrix_mult(M, inv_matrix);

	lib3ds_vector_transform(worldPos, M, vector);
	return Vector(worldPos);
}
