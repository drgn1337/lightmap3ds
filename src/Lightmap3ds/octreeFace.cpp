#include "lightmapFace.h"
#include "minmax.h"
#include "octreeFace.h"

OctreeFace::OctreeFace()
{
	sourceFace = nullptr;
}

const LightmapFace* OctreeFace::GetSourceFace() const
{
	return sourceFace;
}

const Box& OctreeFace::GetBounds() const
{
	return bounds;
}

OctreeFace::BoxTestResult OctreeFace::BoxTest(const Box& box) const
{
	// First we do a simple test: If the range of any of the axes in box are completely
	// beyond the range of that of our bounding box, then they must not intersect in any way
	if (bounds.min.x > box.max.x || bounds.max.x < box.min.x ||
		bounds.min.y > box.max.y || bounds.max.y < box.min.y ||
		bounds.min.z > box.max.z || bounds.max.z < box.min.z)
	{
		return OutOfBounds;
	}

	// Next test: Check if box is completely inside our bounding box
	if (bounds.min.x <= box.min.x && bounds.max.x >= box.max.x &&
		bounds.min.y <= box.min.y && bounds.max.y >= box.max.y &&
		bounds.min.z <= box.min.z && bounds.max.z >= box.max.z)
	{
		return InBounds;
	}

	// If we get here, we know that the box is not completely out of range of
	// the bounding box (test 1), but we also know it's not fully contained
	// therein (test 2). Therefore, it must intersect in some way.
	return Intersects;
}

RaycastHit OctreeFace::Raycast(const Ray& ray) const
{
	return sourceFace->Raycast(ray);
}

OctreeFace* OctreeFace::FromLightmapFace(const LightmapFace* lightmapFace)
{
	OctreeFace *octreeFace = new OctreeFace();
	octreeFace->sourceFace = lightmapFace;
	octreeFace->bounds = lightmapFace->GetWorldBounds();	
	return octreeFace;
}
