#pragma once

// Contains declarations for setup functions specific to baked model rendering

class BakedModel;
struct LightmapBakerSettings;
struct Lib3dsFile;

BakedModel* bake3dsModel(Lib3dsFile *model, const LightmapBakerSettings& settings);
void generateGlLightmapTexturesFromBakedModel(BakedModel* model);
void generateGlListsFromBakedModel(BakedModel* model);
