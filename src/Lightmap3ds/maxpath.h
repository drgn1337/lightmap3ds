#pragma once

// Defines the maximum number of characters allowed in a file path

#ifndef MAX_PATH
#define MAX_PATH	260
#endif
