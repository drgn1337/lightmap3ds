#pragma once

#include "vector.h"

// A geometric box
class Box 
{
public:
	Vector min;
	Vector max;

public:
	Box();
	Box(float minx, float miny, float minz, float maxx, float maxy, float maxz);

public:
	void Zero();
	void Combine(const Box& other);

public:
	Vector GetCenter() const;
	Vector GetSize() const;
};
