#pragma once

// Contains declarations for setup functions specific to unbaked model rendering

struct Lib3dsFile;

void generateGlListsFrom3dsModel(Lib3dsFile* model);