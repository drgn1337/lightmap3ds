#include <lib3ds\light.h>
#include "octree.h"
#include "lightmapPatch.h"
#include "lightmapFace.h"
#include "lightmapVertex.h"
#include "lightmapLight.h"
#include "lightmapMaterial.h"
#include "lightmapLumelBuilder.h"
#include "vectorMath.h"
#include "debugTools.h"

LightmapLumelBuilder::LightmapLumelBuilder(Octree *sourceOctree, std::vector<LightmapLight> sourceLights)
{
	octree = sourceOctree;
	lights = sourceLights;
}

void LightmapLumelBuilder::WriteLumelsToPatch(const LightmapPatch* patch)
{
	const LightmapFace* face = patch->GetManagedFace();

	if (nullptr == face)
	{
		// Ignore this patch; it does not manage a face
	}
	else
	{
		UpdateIntermediateCalculationVariables(face);

		int faceWidth = patch->GetLightmapFaceTextureWidth(face);
		int faceHeight = patch->GetLightmapFaceTextureHeight(face);

		for (int y = patch->GetOriginY(); y < patch->GetOriginY() + faceHeight; y++)
		{
			for (int x = patch->GetOriginX(); x < patch->GetOriginX() + faceWidth; x++)
			{
				WriteLumelToPatch(patch, x, y);
			}
		}
	}

	for (int i = 0; i < LightmapPatch::ChildCount; i++)
	{
		if (nullptr != patch->GetChild(i))
		{
			WriteLumelsToPatch(patch->GetChild(i));
		}
	}
}

void LightmapLumelBuilder::UpdateIntermediateCalculationVariables(const LightmapFace* face)
{
	const LightmapVertex* p = face->GetVertices();

	// Face world coordinates projected on the favored axis
	x0 = p[0].projectedPos.x; y0 = p[0].projectedPos.y;
	x1 = p[1].projectedPos.x; y1 = p[1].projectedPos.y;
	x2 = p[2].projectedPos.x; y2 = p[2].projectedPos.y;

	// Face UV coordinates
	u0 = p[0].lumelPos.u; v0 = p[0].lumelPos.v;
	u1 = p[1].lumelPos.u; v1 = p[1].lumelPos.v;
	u2 = p[2].lumelPos.u; v2 = p[2].lumelPos.v;

	// Denominator
	denominator = (v0 - v2)*(u1 - u2) - (v1 - v2)*(u0 - u2);

	// Dot products
	dpdu = ((x1 - x2)*(v0 - v2) - (x0 - x2)*(v1 - v2)) / denominator;
	dpdv = ((x1 - x2)*(u0 - u2) - (x0 - x2)*(u1 - u2)) / -denominator;
	dqdu = ((y1 - y2)*(v0 - v2) - (y0 - y2)*(v1 - v2)) / denominator;
	dqdv = ((y1 - y2)*(u0 - u2) - (y0 - y2)*(u1 - u2)) / -denominator;
}

void LightmapLumelBuilder::WriteLumelToPatch(const LightmapPatch* patch, int textureX, int textureY)
{
	// Get the color of the lumel
	LightmapColor lumelColor = GetLumelColor(patch->GetManagedFace(), textureX, textureY);

	// Write it to the patch lumel map
	unsigned char* lumelBGR = patch->GetLumels() + (textureY * LightmapPatch::TextureWidth + textureX) * LightmapPatch::BytesPerLumel;
	lumelBGR[0] = LightmapColor::FloatToLumelByte(lumelColor.b);
	lumelBGR[1] = LightmapColor::FloatToLumelByte(lumelColor.g);
	lumelBGR[2] = LightmapColor::FloatToLumelByte(lumelColor.r);
}

bool TexelSegmentsIntersect(const Texel& a1, const Texel& a2, const Texel& b1, const Texel& b2, Texel& intersection)
{
	Texel b = a2 - a1;
	Texel d = b2 - b1;
	float bDotDPerp = b.u * d.v - b.v * d.u;

	// if b dot d == 0, it means the lines are parallel so have infinite intersection points
	if (bDotDPerp == 0)
		return false;

	Texel c = b1 - a1;
	float t = (c.u * d.v - c.v * d.u) / bDotDPerp;
	if (t < 0 || t > 1)
		return false;

	float u = (c.u * b.v - c.v * b.u) / bDotDPerp;
	if (u < 0 || u > 1)
		return false;

	intersection = a1 + b * t;
	return true;
}


Vector LightmapLumelBuilder::GetLumelWorldPos(const LightmapFace* face, const Texel& lumel)
{
	const Plane& plane = face->GetPlane();
	const LightmapVertex* p = face->GetVertices();

	// Calculate the UV position relative to (u0,v0)
	float u = lumel.u - u0;
	float v = lumel.v - v0;

	// Based on the favored axis, calculate the result
	Vector result;
	switch (face->GetFavoredAxis())
	{
	case LightmapFace::Axis::X:
		result.z = (x0)+(dpdu * u) + (dpdv * v);
		result.y = (y0)+(dqdu * u) + (dqdv * v);
		result.x = -(plane.b * result.y + plane.c * result.z + plane.d) / plane.a;
		break;

	case LightmapFace::Axis::Y:
		result.x = (x0)+(dpdu * u) + (dpdv * v);
		result.z = (y0)+(dqdu * u) + (dqdv * v);
		result.y = -(plane.a * result.x + plane.c * result.z + plane.d) / plane.b;
		break;

	case LightmapFace::Axis::Z:
		result.x = (x0)+(dpdu * u) + (dpdv * v);
		result.y = (y0)+(dqdu * u) + (dqdv * v);
		result.z = -(plane.a * result.x + plane.b * result.y + plane.d) / plane.c;
		break;
	}

	return result;
}

Vector LightmapLumelBuilder::GetLumelWorldNorm(const LightmapFace* face, const Vector& worldPos)
{
	const Vector& A = face->GetVertices()[0].worldPos;
	const Vector& B = face->GetVertices()[1].worldPos;
	const Vector& C = face->GetVertices()[2].worldPos;
	const Vector& P = worldPos;
	const Vector N = face->GetNormal();

	// Compute twice area of triangle ABC
	float AreaABC = dotProduct(N, (B - A)*(C - A));

	// Compute a
	float AreaPBC = dotProduct(N, (B - P)*(C - P));
	float a = AreaPBC / AreaABC;

	// Compute b
	float AreaPCA = dotProduct(N, (C - P)*(A - P));
	float b = AreaPCA / AreaABC;

	// Compute c
	float c = 1.0f - a - b;

	// Since we know that vWorld = v1 * lamda1 + v2 * lamda2 + v3 * lamda3,
	// we can also calculate vNorm using the same method with normal vertices.
	const Vector normal =
		face->GetVertices()[0].normal * a
		+ face->GetVertices()[1].normal * b
		+ face->GetVertices()[2].normal * c;
	return normal;
}

LightmapColor LightmapLumelBuilder::GetLumelColor(const LightmapFace* face, int textureX, int textureY)
{
	// Calculate the lumel UV which corresponds to the texture map X,Y position. Notice that
	// we add 0.5; this is because we want to target the center of the 1x1 lumel box 
	// (as opposed to the corner).
	float u = ((float)textureX + 0.5f) / (float)LightmapPatch::TextureWidth;
	float v = ((float)textureY + 0.5f) / (float)LightmapPatch::TextureHeight;

	// Though being a hack, doing imprecise edge testing seems to greatly improve the quality of the rendering. If we set this to zero,
	// then shadows looks correct but faces that should not have shadows have shadowed edges. Being the value it is, non-shadowed faces
	// look correct but the edges of shadowed faces aren't always dark. Conservative rasterization should resolve this but it's not
	// fully correctly implemented at this time
	const float lumelEdgeTestTolerance = -0.01f;

	if (!face->LumelContainedByEdgesWithTolerance(Texel(u, v), lumelEdgeTestTolerance))
	{
		// The center of the lumel is outside the face. Lets use the concept conservative rasterization to pick another lumel position.
		// What we do here is look for where the lumel's bounding box intersects the face, and use a point near that as our uv.
		Texel topLeft((float)textureX / (float)LightmapPatch::TextureWidth, (float)textureY / (float)LightmapPatch::TextureHeight);
		Texel topRight((float)textureX * 0.99f / (float)LightmapPatch::TextureWidth, (float)textureY / (float)LightmapPatch::TextureHeight);
		Texel bottomLeft((float)textureX / (float)LightmapPatch::TextureWidth, (float)textureY * 0.99f / (float)LightmapPatch::TextureHeight);
		Texel bottomRight((float)textureX * 0.99f / (float)LightmapPatch::TextureWidth, (float)textureY * 0.99f / (float)LightmapPatch::TextureHeight);

		bool anyIntersection = false;
		for (int i = 0; i < LightmapFace::VertexCount; i++)
		{
			const LightmapVertex* v1 = face->GetVertices() + i;
			const LightmapVertex* v2 = face->GetVertices() + (i + 1) % LightmapFace::VertexCount;
			Texel intersection;

			if (TexelSegmentsIntersect(v1->lumelPos, v2->lumelPos, topLeft, topRight, intersection))
			{
				u = intersection.u;
				v = intersection.v;
				anyIntersection = true;
			}
			else if (TexelSegmentsIntersect(v1->lumelPos, v2->lumelPos, topRight, bottomRight, intersection))
			{
				u = intersection.u;
				v = intersection.v;
				anyIntersection = true;
			}
			else if (TexelSegmentsIntersect(v1->lumelPos, v2->lumelPos, bottomRight, bottomLeft, intersection))
			{
				u = intersection.u;
				v = intersection.v;
				anyIntersection = true;
			}
			else if (TexelSegmentsIntersect(v1->lumelPos, v2->lumelPos, bottomLeft, topLeft, intersection))
			{
				u = intersection.u;
				v = intersection.v;
				anyIntersection = true;
			}
		}
		if (!anyIntersection)
		{
			// The texel is not in the same space as the face not intersect the face at all
			return LightmapColor(0, 0, 0);
		}
		else
		{
			// If we get here, (u,v) is a point on the edge of the face. We need to move it
			// to within the face by calculating the center lumel positoin of the face and
			// converging (u,v) toward it by 10%
			Texel c;
			for (int i = 0; i < 3; i++)
			{
				c = c + face->GetVertices()[i].lumelPos;
			}
			c = c / 3.0f;
			u = (u * 9.0f + c.u) / 10.0f;
			v = (v * 9.0f + c.v) / 10.0f;
		}
	}

	// Get the lumel position in world space
	Vector worldPos = GetLumelWorldPos(face, Texel(u, v));
	Vector worldNorm = GetLumelWorldNorm(face, worldPos);

	// Calculate the color intensity of all the lights combined on the lumel in world space
	LightmapColor lumelColor;
	for (auto light : lights)
	{
		lumelColor += GetLumelColorForLight(light, face, worldPos, worldNorm);
	}
	// Ensure the color components are between 0 and 1
	lumelColor.Clamp();
	return lumelColor;
}

LightmapColor LightmapLumelBuilder::GetLumelColorForLight(const LightmapLight& light, const LightmapFace* face, const Vector& pos, const Vector& norm)
{
	const LightmapMaterial* mat = face->GetSourceMaterial();
	Vector lightPos = light.position;
	Vector incident = (pos - lightPos).Normalized();
	float dp = dotProduct(norm, -incident);
	float spotEffect = 1; // The relevance of the spotlight at this particular lumel. 1 means fully lit, 0 means completely outside the cone

	if (dp <= 0)
	{
		// The face is facing away from the light source
		return LightmapColor(0, 0, 0);
	}
	else
	{
		// The function that calls this one already established that the light ray hits the face. We need to get the distance
		// the ray travelled to compare with other faces to see if theirs are closer to the ray
		Ray lightRay(lightPos, incident);
		RaycastHit faceHit = face->RaycastFacePlane(lightRay);
		
		// Test that the light hits no other face
		if (face->PointContainedByEdges(faceHit.point))
		{
			// Now see if the light ray hits another face first
			RaycastHit octreeHit = octree->Raycast(RaycastSettings(lightRay, face));
			if (octreeHit.didHit && octreeHit.distance < faceHit.distance)
			{
				// Another face is obstructing this one from the light ray, cast a shadow
				return LightmapColor(0, 0, 0);
			}
		}
		else
		{
			// The light ray does not hit this face (although it does hit its plane); therefore it cannot cast a shadow
		}

		// Adjust for hotspots
		if (LightmapLight::LightType::Spot == light.type)
		{
			spotEffect = dotProduct(light.spotDirection, incident);

			if (spotEffect < light.cutoffCosine)
			{
				// This light ray has no effect because it's outside the spotlight
				spotEffect = 0;
			}
			else if (spotEffect < light.hotspotCosine)
			{
				// Approach zero as spotEffect approaches cutoffCosine
				spotEffect *= (spotEffect - light.cutoffCosine) / (light.hotspotCosine - light.cutoffCosine);
			}
		}

		// Calculate the luminosity. Currently only diffuse shading is supported
		LightmapColor diffuse = (nullptr == mat ? LightmapColor(1,1,1) : mat->diffuse) * light.color * dp * spotEffect;
		return diffuse;
	}
}
