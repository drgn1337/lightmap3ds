#pragma once

// Contains all of the lightmap baker settings
struct LightmapBakerSettings
{
	// The quality (that is, how much we scale each face on the lightmap texture).
	// Default value is 1 meaning if a face is 5x5 units large in world space, it
	// it will take up a 5x5 region on the light map. A value of 2 would cause
	// that same face to take up a 10x10 region on the light map.
	float quality;

	// True if we should blur the lightmap before rendering
	bool blur;

	// True if we should force all the faces for the entire model to be shaded
	// smoothly by recalculating the normals of their vertices
	bool forceSmoothing;

	LightmapBakerSettings()
	{
		quality = 1;
		blur = false;
		forceSmoothing = false;
	}

	static LightmapBakerSettings SettingsFromCommandLine(int argc, char *argv[]);
};