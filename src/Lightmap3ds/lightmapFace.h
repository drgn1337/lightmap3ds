#pragma once

struct Lib3dsMesh;
struct Lib3dsFace;
struct Lib3dsNode;

struct LightmapMaterial;

#include <vector>
#include <lib3ds\vector.h>
#include "vector.h"
#include "box.h"
#include "rectangle.h"
#include "plane.h"
#include "ray.h"
#include "raycastHit.h"
#include "lightmapVertex.h"

// Acts as a container for a face in a 3ds model. This object is used 
// to calculate and retain information regarding how to render a lightmap
// texture onto it
class LightmapFace
{
public:
	enum Axis { X, Y, Z };

public:
	const static int VertexCount = 3;

private:
	// The 3ds face that this face is based off
	Lib3dsFace* sourceFace;
	// The source face material
	LightmapMaterial* sourceMaterial;

private:
	// The face vertices
	LightmapVertex vertices[LightmapFace::VertexCount];
	// The plane containing the face
	Plane facePlane;
	// The planes containing each edge of the face pointing away from the center of the face
	std::vector<Plane> edgePlanes;
	// The planes containing each lumel of the face pointing away from the center of the face
	std::vector<Plane> lightmapEdgePlanes;
	// The bounding box of the face in world space
	Box bounds;

	// Identifies which axe that this face has the largest presence in
	Axis favoredAxis;
	// The bounding rectangle of the face as it is projected on its favored axis
	class Rectangle projectionBounds;
	// The area of the face as it is projected onto its favored axis
	float projectionArea;

private:
	// Assigns world position values to vertices
	void BuildVertices(Lib3dsNode* sourceNode, Lib3dsMesh* sourceMesh, Lib3dsFace* sourceFace);
	// Assigns world normal values to vertices
	void BuildNormals(Lib3dsMesh* sourceMesh, Lib3dsVector *sourceMeshNormals);
	// Populates the facePlane with world data
	void BuildPlane();
	// Populates edgePlanes with world data
	void BuildEdgePlanes();
	void BuildEdgePlane(const Vector& v1, const Vector& v2);
	// Populates bounds with world data
	void BuildBounds();
	// Assigns the axis which this face takes up the most space in
	void BuildFavoredAxis();
	// Assigns the vertices projected onto the favored axis
	void BuildProjectionVertices();
	// Assigns the bounds of the face projected onto the favored axis
	void BuildProjectionBounds();

public:
	// Populates lightmap UV position values to vertices
	void BuildLightmapUVs(int textureOriginX, int textureOriginY, float textureQualityScaleFactor);
	// Populates lightmapEdgePlanes
	void BuildLightmapEdgePlanes();
	void BuildLightmapEdgePlane(const Texel& t1, const Texel& t2);

public:
	// Get the source face
	const Lib3dsFace* GetSourceFace() const;

	// Get the material
	const LightmapMaterial* GetSourceMaterial() const;

	// Get the vertices
	const LightmapVertex* GetVertices() const;

	// Get the normal of the face in world space
	Vector GetNormal() const;

	// Get the plane containing the face
	const Plane& GetPlane() const;

	// Get the edge planes
	const std::vector<Plane> GetEdgePlanes() const;

	// Get the lumel edge planes
	const std::vector<Plane> GetLumelEdgePlanes() const;

	// Get the area of the face in world space
	float GetArea() const;

	// Get the axis that this face has the largest presence in
	Axis GetFavoredAxis() const;

	// Gets the bounding box of the face in world coodinates
	const Box GetWorldBounds() const;

	// Get the projection bounds
	const class Rectangle& GetProjectionBounds() const;

	// Get the width of the face as it is projected on its favored axis
	float GetProjectionWidth() const;

	// Get the height of the face as it is projected on its favored axis
	float GetProjectionHeight() const;

public:
	// Determines if the face intersects the plane of the face
	RaycastHit RaycastFacePlane(const Ray& ray) const;

	// Determines if the face intersects a ray
	RaycastHit Raycast(const Ray& ray) const;

	// Determines if a point is contained within the face edges
	bool PointContainedByEdges(const Vector& point) const;

	// Determines if a point is approximately contained within the face edges
	bool PointContainedByEdgesWithTolerance(const Vector& point, float tolerance) const;

	// Determines if a lumel is approximately contained within the face edges
	bool LumelContainedByEdgesWithTolerance(const Texel& lumel, float tolerance) const;

public:
	static LightmapFace* From3dsFace(Lib3dsNode* sourceNode, Lib3dsMesh* sourceMesh, 
		Lib3dsVector *sourceMeshNormals, Lib3dsFace* sourceFace, LightmapMaterial* sourceMaterial);
};
