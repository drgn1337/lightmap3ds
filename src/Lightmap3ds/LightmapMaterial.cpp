#include <lib3ds\material.h>
#include "lightmapMaterial.h"

LightmapMaterial LightmapMaterial::From3dsMaterial(const Lib3dsMaterial* sourceMaterial)
{
	LightmapMaterial mat;
	mat.textureHandle = (int)sourceMaterial->texture1_map.user.p;
	if (0 == mat.textureHandle)
	{
		mat.diffuse = LightmapColor(sourceMaterial->diffuse);
	}
	else
	{
		mat.diffuse = LightmapColor(1, 1, 1);
	}
	return mat;
}
