#pragma once

// Contains functions for initializing Glut for rendering as well
// as helper functions used for navigation

void initializeGlutSimulation(int argc, char *argv[]);

bool isLeftMouseButtonDown();
bool isRightMouseButtonDown();
bool isMiddleMouseButtonDown();

int getMouseX();
int getMouseY();

int getWindowWidth();
int getWindowHeight();
