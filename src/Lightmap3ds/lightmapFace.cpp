#include <lib3ds\mesh.h>
#include <lib3ds\file.h>
#include <lib3ds\light.h>
#include <lib3ds\matrix.h>
#include <lib3ds\node.h>
#include <lib3ds\vector.h>
#include "lightmapFace.h"
#include "lightmapPatch.h"
#include "vectorMath.h"
#include "minmax.h"

void LightmapFace::BuildVertices(Lib3dsNode* sourceNode, Lib3dsMesh* sourceMesh, Lib3dsFace* sourceFace)
{
	// Transform the raw 3ds points into world space
	Lib3dsMatrix inv_matrix, M;
	Lib3dsVector v;

	lib3ds_matrix_copy(inv_matrix, sourceMesh->matrix);
	lib3ds_matrix_inv(inv_matrix);
	lib3ds_matrix_copy(M, sourceNode->matrix);
	lib3ds_matrix_translate_xyz(M, -sourceNode->data.object.pivot[0], -sourceNode->data.object.pivot[1], -sourceNode->data.object.pivot[2]);
	lib3ds_matrix_mult(M, inv_matrix);

	for (int i = 0; i < VertexCount; i++)
	{
		lib3ds_vector_transform(v, M, sourceMesh->pointL[sourceFace->points[i]].pos);
		vertices[i].worldPos = Vector::From3dsVector(v);
		if (sourceMesh->texels > 0)
		{
			vertices[i].texelPos = Texel::From3dsTexel(sourceMesh->texelL[sourceFace->points[i]]);
		}
	}
}

void LightmapFace::BuildNormals(Lib3dsMesh* sourceMesh, Lib3dsVector *sourceMeshNormals)
{
	// Assign lightmap vertex normals based on calculations done from 3ds mesh smoothing parameters.
	// To access the normal of the i-th vertex of the j-th face we access sourceMeshNormals[3*j+i]

	int faceIndex = (int)(sourceFace - sourceMesh->faceL);
	for (int i = 0; i < VertexCount; i++)
	{
		vertices[i].normal = Vector::From3dsVector(sourceMeshNormals[faceIndex * 3 + i]);
	}
}

void LightmapFace::BuildPlane()
{
	Vector unitEdge1 = (vertices[1].worldPos - vertices[0].worldPos).Normalized();
	Vector unitEdge2 = (vertices[2].worldPos - vertices[0].worldPos).Normalized();
	Vector normal = (unitEdge1 * unitEdge2).Normalized();

	facePlane.a = normal.x;
	facePlane.b = normal.y;
	facePlane.c = normal.z;
	facePlane.d = -(facePlane.a * vertices[0].worldPos.x + facePlane.b * vertices[0].worldPos.y + facePlane.c * vertices[0].worldPos.z);
}

void LightmapFace::BuildEdgePlanes()
{
	BuildEdgePlane(vertices[0].worldPos, vertices[1].worldPos);
	BuildEdgePlane(vertices[1].worldPos, vertices[2].worldPos);
	BuildEdgePlane(vertices[2].worldPos, vertices[0].worldPos);
}

void LightmapFace::BuildEdgePlane(const Vector& v1, const Vector& v2)
{
	Vector edge = v2 - v1;
	Vector edgeNormal = (GetNormal() * edge.Normalized()).Normalized();
	Plane plane;
	plane.a = edgeNormal.x;
	plane.b = edgeNormal.y;
	plane.c = edgeNormal.z;
	plane.d = -(plane.a * v1.x + plane.b * v1.y + plane.c * v1.z);
	edgePlanes.push_back(plane);
}

void LightmapFace::BuildBounds()
{
	bounds.min = bounds.max = vertices[0].worldPos;

	for (int i = 1; i < VertexCount; i++)
	{
		Vector& v = vertices[i].worldPos;
		bounds.min.x = min(bounds.min.x, v.x);
		bounds.min.y = min(bounds.min.y, v.y);
		bounds.min.z = min(bounds.min.z, v.z);
		bounds.max.x = max(bounds.max.x, v.x);
		bounds.max.y = max(bounds.max.y, v.y);
		bounds.max.z = max(bounds.max.z, v.z);
	}
}

void LightmapFace::BuildFavoredAxis()
{
	Vector absoluteFaceNormal(fabs(facePlane.a), fabs(facePlane.b), fabs(facePlane.c));
	if (absoluteFaceNormal.x >= absoluteFaceNormal.y && absoluteFaceNormal.x >= absoluteFaceNormal.z)
	{
		favoredAxis = Axis::X;
	}
	else if (absoluteFaceNormal.y >= absoluteFaceNormal.x && absoluteFaceNormal.y >= absoluteFaceNormal.z)
	{
		favoredAxis = Axis::Y;
	}
	else
	{
		favoredAxis = Axis::Z;
	}
}

void LightmapFace::BuildProjectionVertices()
{
	Vector a(vertices[0].worldPos);
	Vector b(vertices[1].worldPos);
	Vector c(vertices[2].worldPos);

	switch (favoredAxis)
	{
	case Axis::X:
		// This face favors the X-axis
		vertices[0].projectedPos = Vector(a.z, a.y, 0);
		vertices[1].projectedPos = Vector(b.z, b.y, 0);
		vertices[2].projectedPos = Vector(c.z, c.y, 0);
		break;

	case Axis::Y:
		// This face favors the Y-axis
		vertices[0].projectedPos = Vector(a.x, a.z, 0);
		vertices[1].projectedPos = Vector(b.x, b.z, 0);
		vertices[2].projectedPos = Vector(c.x, c.z, 0);
		break;

	case Axis::Z:
		// This face favors the Z-axis
		vertices[0].projectedPos = Vector(a.x, a.y, 0);
		vertices[1].projectedPos = Vector(b.x, b.y, 0);
		vertices[2].projectedPos = Vector(c.x, c.y, 0);
		break;
	}
}

void LightmapFace::BuildProjectionBounds()
{
	switch (favoredAxis)
	{
	case Axis::X:
		// This face favors the X-axis
		projectionBounds.min.x = bounds.min.z;
		projectionBounds.max.x = bounds.max.z;
		projectionBounds.min.y = bounds.min.y;
		projectionBounds.max.y = bounds.max.y;
		break;

	case Axis::Y:
		// This face favors the Y-axis
		projectionBounds.min.x = bounds.min.x;
		projectionBounds.max.x = bounds.max.x;
		projectionBounds.min.y = bounds.min.z;
		projectionBounds.max.y = bounds.max.z;
		break;

	case Axis::Z:
		// This face favors the Z-axis
		projectionBounds.min.x = bounds.min.x;
		projectionBounds.max.x = bounds.max.x;
		projectionBounds.min.y = bounds.min.y;
		projectionBounds.max.y = bounds.max.y;
		break;
	}
}

void LightmapFace::BuildLightmapUVs(int textureOriginX, int textureOriginY, float textureQualityScaleFactor)
{
	for (int i = 0; i < VertexCount; i++)
	{
		LightmapVertex& point = vertices[i];

		// Calculating the u and v relative to the origin of the bounds of
		// the projection coordinates
		point.lumelPos.u = (point.projectedPos.x - projectionBounds.min.x) * textureQualityScaleFactor;
		point.lumelPos.v = (point.projectedPos.y - projectionBounds.min.y) * textureQualityScaleFactor;

		// Now offset the lumel by the origin of this bounding light rectangle
		// Also factor in the buffer area 
		point.lumelPos.u += textureOriginX;
		point.lumelPos.v += textureOriginY;

		// Now transform to the texture [0,1] range
		point.lumelPos.u /= (float)LightmapPatch::TextureWidth;
		point.lumelPos.v /= (float)LightmapPatch::TextureHeight;
	}
}

void LightmapFace::BuildLightmapEdgePlanes()
{
	Texel unitEdge1 = (vertices[1].lumelPos - vertices[0].lumelPos).Normalized();
	Texel unitEdge2 = (vertices[2].lumelPos - vertices[0].lumelPos).Normalized();
	float cross = (unitEdge1.u * unitEdge2.v) - (unitEdge1.v * unitEdge2.u);

	if (cross < 0)
	{
		BuildLightmapEdgePlane(vertices[0].lumelPos, vertices[2].lumelPos);
		BuildLightmapEdgePlane(vertices[2].lumelPos, vertices[1].lumelPos);
		BuildLightmapEdgePlane(vertices[1].lumelPos, vertices[0].lumelPos);
	}
	else
	{
		BuildLightmapEdgePlane(vertices[0].lumelPos, vertices[1].lumelPos);
		BuildLightmapEdgePlane(vertices[1].lumelPos, vertices[2].lumelPos);
		BuildLightmapEdgePlane(vertices[2].lumelPos, vertices[0].lumelPos);
	}
}

void LightmapFace::BuildLightmapEdgePlane(const Texel& t1, const Texel& t2)
{
	Texel edge = t2 - t1;
	Texel edgeNormal = Texel(-edge.v, edge.u).Normalized();
	Plane plane;
	plane.a = edgeNormal.u;
	plane.b = edgeNormal.v;
	plane.c = -(plane.a * t1.u + plane.b * t1.v);
	plane.d = 0;
	lightmapEdgePlanes.push_back(plane);
}

const Lib3dsFace* LightmapFace::GetSourceFace() const
{
	return sourceFace;
}

const LightmapMaterial* LightmapFace::GetSourceMaterial() const
{
	return sourceMaterial;
}

const LightmapVertex* LightmapFace::GetVertices() const
{
	return vertices;
}

Vector LightmapFace::GetNormal() const
{
	return Vector(facePlane.a, facePlane.b, facePlane.c);
}

const Plane& LightmapFace::GetPlane() const
{
	return facePlane;
}

const std::vector<Plane> LightmapFace::GetEdgePlanes() const
{
	return edgePlanes;
}

const std::vector<Plane> LightmapFace::GetLumelEdgePlanes() const
{
	return lightmapEdgePlanes;
}

float LightmapFace::GetArea() const
{
	const Vector& a = vertices[0].worldPos;
	const Vector& b = vertices[1].worldPos;
	const Vector& c = vertices[2].worldPos;
	const Vector n(GetNormal());
	float area = dotProduct(n, (b - a)*(c - a)) / 2.0f;
	return area;
}

LightmapFace::Axis LightmapFace::GetFavoredAxis() const
{
	return favoredAxis;
}

const Box LightmapFace::GetWorldBounds() const
{
	return bounds;
}

const Rectangle& LightmapFace::GetProjectionBounds() const
{
	return projectionBounds;
}

float LightmapFace::GetProjectionWidth() const
{
	return projectionBounds.max.x - projectionBounds.min.x;
}

float LightmapFace::GetProjectionHeight() const
{
	return projectionBounds.max.y - projectionBounds.min.y;
}

RaycastHit LightmapFace::RaycastFacePlane(const Ray& ray) const
{
	RaycastHit result;

	// Intersection = t = -(Ax1 + By1 + Cz1 + D) / (Ai + Bj + Ck)
	// where the plane containing the polygon is <A,B,C,D> and the
	// origin of the ray is (x1,y1,z1) and the vector of the ray
	// is <i,j,k>
	float denom = facePlane.a * ray.direction.x + facePlane.b * ray.direction.y + facePlane.c * ray.direction.z;

	// Fail if the ray never sees the front of the face
	if (denom > 0)
	{
		return result;
	}

	// Get the distance from the ray to the face plane
	float distance;
	if (denom != 0.0f)
	{
		distance = -(facePlane.a * ray.origin.x + facePlane.b * ray.origin.y + facePlane.c * ray.origin.z + facePlane.d) / denom;
	}
	else
	{
		distance = -1;
	}

	// Fail if the distance is a negative value
	if (distance <= 0.0f)
	{
		return result;
	}

	// Get the intersection point
	result.didHit = true;
	result.distance = distance;
	result.point = ray.origin + ray.direction * distance;
	return result;
}

RaycastHit LightmapFace::Raycast(const Ray& ray) const
{
	// Test the ray against this face's plane
	RaycastHit planeResult = RaycastFacePlane(ray);
	RaycastHit noHitResult;

	if (planeResult.didHit)
	{
		// The ray hit the face plane. Make sure the intersection point is in the bounds of the face
		if (PointContainedByEdges(planeResult.point))
		{
			// If we get here the ray definitely hits the face
			return planeResult;
		}
	}
	else
	{
		// The ray did not hit the face plane
	}

	// If we get here then the ray does not hit the face
	return noHitResult;
}

bool LightmapFace::PointContainedByEdges(const Vector& point) const
{
	return PointContainedByEdgesWithTolerance(point, 0.0f);
}

bool LightmapFace::PointContainedByEdgesWithTolerance(const Vector& point, float tolerance) const
{
	for (auto edgePlane : edgePlanes)
	{
		float d = point.x * edgePlane.a + point.y * edgePlane.b + point.z * edgePlane.c + edgePlane.d;
		if (d < tolerance)
		{
			return false;
		}
	}
	return true;
}

bool LightmapFace::LumelContainedByEdgesWithTolerance(const Texel& lumel, float tolerance) const
{
	for (auto edgePlane : lightmapEdgePlanes)
	{
		float d = lumel.u * edgePlane.a + lumel.v * edgePlane.b + edgePlane.c;
		if (d < tolerance)
		{
			return false;
		}
	}
	return true;
}

LightmapFace* LightmapFace::From3dsFace(Lib3dsNode* sourceNode, Lib3dsMesh* sourceMesh, 
	Lib3dsVector *sourceMeshNormals, Lib3dsFace* sourceFace, LightmapMaterial* sourceMaterial)
{
	LightmapFace* face = new LightmapFace();
	face->sourceFace = sourceFace;
	face->sourceMaterial = sourceMaterial;
	face->BuildVertices(sourceNode, sourceMesh, sourceFace);
	face->BuildNormals(sourceMesh, sourceMeshNormals);
	face->BuildPlane();
	face->BuildEdgePlanes();
	face->BuildBounds();
	face->BuildFavoredAxis();
	face->BuildProjectionVertices();
	face->BuildProjectionBounds();
	return face;
}
