#include <algorithm>
#include <lib3ds\file.h>
#include <lib3ds\mesh.h>
#include <lib3ds\node.h>
#include <lib3ds\light.h>
#include <lib3ds\material.h>
#include "lightmapBakerSettings.h"
#include "lightmapBaker.h"
#include "lightmapFace.h"
#include "lightmapPatch.h"
#include "lightmapLight.h"
#include "lightmapMaterial.h"
#include "lightmapLumelBuilder.h"
#include "bakedModel.h"
#include "octree.h"
#include "minmax.h"
#include "debugTools.h"

LightmapBaker::LightmapBaker(Lib3dsFile *fileToBake, const LightmapBakerSettings& settings)
	: settings(settings)
{
	this->fileToBake = fileToBake;
}

LightmapBaker::~LightmapBaker()
{
	if (nullptr != octree)
	{
		delete octree;
	}
}

void LightmapBaker::BuildOctree()
{
	octree = Octree::FromLightmapFaces(faces);
}

void LightmapBaker::BuildLightmapMaterials()
{
	for (Lib3dsMaterial* sourceMat = fileToBake->materials; nullptr != sourceMat; sourceMat = sourceMat->next)
	{
		LightmapMaterial* mat = new LightmapMaterial(LightmapMaterial::From3dsMaterial(sourceMat));
		materials.insert(std::map<Lib3dsMaterial*, LightmapMaterial*>::value_type(sourceMat, mat));
	}
}

void LightmapBaker::BuildLightmapFaces()
{
	for (Lib3dsNode* node = fileToBake->nodes; node != nullptr; node = node->next)
	{
		BuildLightmapFacesFrom3dsNode(node);
	}
}

void LightmapBaker::BuildLightmapFacesFrom3dsNode(Lib3dsNode* sourceNode)
{
	for (Lib3dsNode* child = sourceNode->childs; child != NULL; child = child->next)
	{
		BuildLightmapFacesFrom3dsNode(child);
	}

	if (sourceNode->type == LIB3DS_OBJECT_NODE)
	{
		Lib3dsMesh *mesh;

		if (strcmp(sourceNode->name, "$$$DUMMY") == 0)
		{
			return;
		}

		mesh = lib3ds_file_mesh_by_name(fileToBake, sourceNode->data.object.morph);
		if (nullptr == mesh)
		{
			mesh = lib3ds_file_mesh_by_name(fileToBake, sourceNode->name);
		}

		if (settings.forceSmoothing)
		{
			for (Lib3dsDword p = 0; p < mesh->faces; ++p)
			{
				mesh->faceL[p].smoothing = true;
			}
		}

		Lib3dsVector *meshNormals = new Lib3dsVector[3 * mesh->faces];
		lib3ds_mesh_calculate_normals(mesh, meshNormals);

		for (Lib3dsDword p = 0; p < mesh->faces; ++p)
		{
			BuildLightmapFaceFrom3dsFace(sourceNode, mesh, meshNormals, &mesh->faceL[p]);
		}

		delete[] meshNormals;
	}
}

void LightmapBaker::BuildLightmapFaceFrom3dsFace(Lib3dsNode* sourceNode, Lib3dsMesh* sourceMesh, Lib3dsVector *sourceMeshNormals, Lib3dsFace* sourceFace)
{
	Lib3dsMaterial* sourceMat = lib3ds_file_material_by_name(fileToBake, sourceFace->material);
	LightmapFace* face = LightmapFace::From3dsFace(sourceNode, sourceMesh, sourceMeshNormals, sourceFace, materials[sourceMat]);

	// Ensure the face has a non-zero area. If it doesn't, we can't use the face.
	if (face->GetArea() < 0.0001f)
	{
		// Too small to map
	}
	else
	{
		faces.push_back(face);
	}
}

void LightmapBaker::SortLightmapFacesByProjectionArea()
{
	// Make it so the faces taking up the most space projected onto their ideal axe are first
	// in the collection, and the ones taking up the least space are last
	std::sort(faces.begin(), faces.end(), [](const LightmapFace* a, const LightmapFace* b) -> bool
	{
		return a->GetProjectionWidth()*a->GetProjectionHeight() > b->GetProjectionWidth()*b->GetProjectionHeight();
	});
}

void LightmapBaker::InitializeLightmaps()
{
	if (faces.size() > 0)
	{
		std::vector<LightmapFace*>::iterator it = faces.begin();
		int facesAdded = 0;

		while (it != faces.end())
		{
			LightmapPatch* lightmap = LightmapPatch::Create(settings.quality);

			if (LightmapPatch::AddFaceResult::FaceTooBig == lightmap->TrySetFace(*it))
			{
				// The face is simply too big to fit into a single texture and therefore will not
				// be lightmapped. In future implementations this can be remedied by reducing the UV
				// area of the face (thereby reducing quality) or by increasing the maximum texture size
			}
			else
			{
				facesAdded++;
			}

			for (++it; it != faces.end(); ++it)
			{
				if (LightmapPatch::AddFaceResult::FaceTooBig == lightmap->TrySetFace(*it))
				{
					// The face is too big to fit into the unreserved texture space. We will not attempt
					// will not attempt to add any more faces to this texture; instead we will start a new one
					break;
				}
				else
				{
					facesAdded++;
				}
			}

			// Add the texture to the master collection
			lightmapPatches.push_back(lightmap);
		}
	}
}

void LightmapBaker::BlurLightmaps()
{
	for (auto lightmap : lightmapPatches)
	{
		lightmap->Blur();
	}
}

void LightmapBaker::WriteLumelsToLightmaps()
{
	// Collect all the scene lights into an array for easy access
	std::vector<LightmapLight> modelLights;
	for (Lib3dsLight* light = fileToBake->lights; nullptr != light; light = light->next)
	{
		if (!light->off)
		{
			modelLights.push_back(LightmapLight::From3dsLight(light));
		}
	}

	// Write lumels to all the lightmaps
	LightmapLumelBuilder builder(octree, modelLights);
	for (auto lightmap : lightmapPatches)
	{
		builder.WriteLumelsToPatch(lightmap);
	}
}

BakedModel* LightmapBaker::Bake(Lib3dsFile *file, const LightmapBakerSettings& settings)
{
	LightmapBaker *baker = new LightmapBaker(file, settings);

	// Loads 3ds material info information into the lightmap builder
	DebugTools::Log("Pre-heating the oven (reading material data from the model)...\n");
	baker->BuildLightmapMaterials();

	// Build the collection of lightmap faces which we will use for baking
	// and updating the model faces with
	DebugTools::Log("Creaming the butter and sugar (building lightmap faces)...\n");
	baker->BuildLightmapFaces();

	// Sort the lightmap face collection from largest to smallest projection area
	// for easy size-based traversal and efficient lightmap texture packing
	DebugTools::Log("Greasing the pan (sorting lightmap faces)...\n");
	baker->SortLightmapFacesByProjectionArea();

	// Generate a collection of blank lightmap textures and patches to house each
	// lightmap face along with information on how to generate lumels for the faces
	DebugTools::Log("Adding eggs and vanilla (allocating light maps and textures)...\n");
	baker->InitializeLightmaps();

	// Create a domain of boxes which encapsulate the model so that
	// we can peform rapid raycasts when we build the light map
	DebugTools::Log("Pouring the batter (building the octree)...\n");
	baker->BuildOctree();

	// At this point in time we know how many textures we're creating, how big they are,
	// the position and texture of every triangle, and the means to convert the triangle
	// from world coordinates to UV coordinates. Now we're ready to populate every lumel
	// in our light maps
	DebugTools::Log("Baking the cake (writing lumels)...\n");
	baker->WriteLumelsToLightmaps();

	// Blur the lightmaps for better quality
	if (settings.blur)
	{
		DebugTools::Log("Applying the frosting (blurring)...\n");
		baker->BlurLightmaps();
	}

	// Assign the materials and lightmaps to the output model
	std::vector<LightmapMaterial*> materials;
	for (auto it = baker->materials.begin(); it != baker->materials.end(); ++it) 
	{
		materials.push_back(it->second);
	}
	BakedModel* model = new BakedModel(file, materials, baker->lightmapPatches);
	DebugTools::Log("Your cake, uh, I mean lightmaps are ready! The cake is a lie.\n");

	// All done with the baker
	delete baker;

	// Return the model
	return model;
}
