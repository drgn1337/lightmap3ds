#include <lib3ds/file.h>
#include <lib3ds/node.h>
#include <lib3ds/mesh.h>
#include <lib3ds/light.h>
#include <lib3ds/material.h>
#include <lib3ds/matrix.h>
#include <GL/GLUT.H>
#include <CxImage/ximage.h>
#include <memory>
#include "minmax.h"
#include "lightmapBaker.h"
#include "lightmapPatch.h"
#include "lightmapFace.h"
#include "lightmapVertex.h"
#include "bakedModel.h"
#include "rendererSetup.h"
#include "maxpath.h"

Lib3dsFile* load3dsModel(const char* szFilename)
{
	Lib3dsFile* file = lib3ds_file_load(szFilename);

	/* No nodes?  Fabricate nodes to display all the meshes. */
	if (!file->nodes)
	{
		Lib3dsMesh *mesh;
		Lib3dsNode *node;

		for (mesh = file->meshes; mesh != NULL; mesh = mesh->next)
		{
			node = lib3ds_node_new_object();
			strcpy_s(node->name, 64, mesh->name);
			node->parent_id = LIB3DS_NO_PARENT;
			lib3ds_file_insert_node(file, node);
		}
	}

	Lib3dsVector bmin, bmax;
	float size, sx, sy, sz, cx, cy, cz;

	lib3ds_file_eval(file, 1.0f);
	lib3ds_file_bounding_box_of_nodes(file, LIB3DS_TRUE, LIB3DS_FALSE, LIB3DS_FALSE, bmin, bmax);
	sx = bmax[0] - bmin[0];
	sy = bmax[1] - bmin[1];
	sz = bmax[2] - bmin[2];
	size = max(sx, sy); size = max(size, sz);
	cx = (bmin[0] + bmax[0]) / 2;
	cy = (bmin[1] + bmax[1]) / 2;
	cz = (bmin[2] + bmax[2]) / 2;

	// No lights in the file?  Add one
	if (NULL == file->lights)
	{
		Lib3dsLight *light;

		light = lib3ds_light_new("light");
		light->spot_light = 0;
		light->see_cone = 0;
		light->color[0] = light->color[1] = light->color[2] = 1;
		light->position[0] = cx;
		light->position[1] = cy + size;
		light->position[2] = cz + size;
		light->position[3] = 0.;
		light->outer_range = 100;
		light->inner_range = 10;
		light->multiplier = 1;
		lib3ds_file_insert_light(file, light);
	}

	return file;
}

void decompose_datapath(const char *fn, char* filename, char* datapath)
{
	const char *ptr = strrchr(fn, '/');
	if (NULL == ptr)
	{
		ptr = strrchr(fn, '\\');
	}

	if (ptr == NULL) {
		strcpy_s(datapath, MAX_PATH, ".");
		strcpy_s(filename, MAX_PATH, fn);
	}
	else {
		strcpy_s(filename, MAX_PATH, ptr + 1);
		strcpy_s(datapath, MAX_PATH, fn);
		datapath[ptr - fn] = '\0';
	}
}

void ensureImageDimsArePowersOfTwo(CxImage* pImage)
{
	unsigned long nOriginalWidth = pImage->GetWidth();
	unsigned long nOriginalHeight = pImage->GetHeight();
	unsigned long nWidth = 1, nHeight = 1;
	while (nWidth < nOriginalWidth && nWidth < 2048) {
		nWidth <<= 1;
	}
	while (nHeight < nOriginalHeight && nHeight < 2048) {
		nHeight <<= 1;
	}
	if (nWidth > 2048) {
		nWidth = 2048;
	}
	if (nHeight > 2048) {
		nHeight = 2048;
	}

	if (nWidth != nOriginalWidth || nHeight != nOriginalHeight)
	{
		pImage->Resample2(nWidth, nHeight);
	}
}

void load3dsModelTextures(const char* szFilename, Lib3dsFile* model, Lib3dsMesh* mesh)
{
	Lib3dsMaterial *oldmat = (Lib3dsMaterial *)-1;
	for (unsigned p = 0; p < mesh->faces; ++p)
	{
		Lib3dsFace *f = &mesh->faceL[p];
		Lib3dsMaterial *mat = nullptr;
		if (f->material[0])
		{
			mat = lib3ds_file_material_by_name(model, f->material);
		}

		if (mat != oldmat)
		{
			if (mat && mat->texture1_map.name[0])
			{
				Lib3dsTextureMap *tex = &mat->texture1_map;
				if (nullptr == tex->user.p) /* no player texture yet? */
				{
					char texname[1024];
					char filename[MAX_PATH];
					char datapath[MAX_PATH];
					decompose_datapath(szFilename, filename, datapath);
					strcpy_s(texname, 1024, datapath);
					strcat_s(texname, 1024, "\\");
					strcat_s(texname, 1024, tex->name);

					CxImage* pImage = new CxImage(texname, CXIMAGE_FORMAT_UNKNOWN);
					if (NULL != pImage)
					{
						ensureImageDimsArePowersOfTwo(pImage);

						glGenTextures(1, (GLuint*)&tex->user.p);
						glBindTexture(GL_TEXTURE_2D, (GLuint)tex->user.p);
						glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
						glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
						glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
						glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
						if (24 == pImage->GetBpp())
						{
							glTexImage2D(GL_TEXTURE_2D, 0, 3, pImage->GetWidth(), pImage->GetHeight(), 0, GL_BGR_EXT, GL_UNSIGNED_BYTE, pImage->GetBits());
							gluBuild2DMipmaps(GL_TEXTURE_2D, 3, pImage->GetWidth(), pImage->GetHeight(), GL_BGR_EXT, GL_UNSIGNED_BYTE, pImage->GetBits());
						}
						else {
							glTexImage2D(GL_TEXTURE_2D, 0, 4, pImage->GetWidth(), pImage->GetHeight(), 0, GL_BGRA_EXT, GL_UNSIGNED_BYTE, pImage->GetBits());
							gluBuild2DMipmaps(GL_TEXTURE_2D, 4, pImage->GetWidth(), pImage->GetHeight(), GL_BGRA_EXT, GL_UNSIGNED_BYTE, pImage->GetBits());
						}

						delete pImage;
					}
					else
					{
						fputs("3dsplayer: Warning: Failed to load texture.\n", stderr);
					}
				}
			}
		}
	}
}

void load3dsModelTextures(const char* szFilename, Lib3dsFile* model, Lib3dsNode* node)
{
	for (Lib3dsNode* child = node->childs; child != NULL; child = child->next)
	{
		load3dsModelTextures(szFilename, model, child);
	}

	if (node->type == LIB3DS_OBJECT_NODE)
	{
		Lib3dsMesh *mesh;

		if (strcmp(node->name, "$$$DUMMY") == 0) {
			return;
		}

		mesh = lib3ds_file_mesh_by_name(model, node->data.object.morph);
		if (mesh == NULL)
			mesh = lib3ds_file_mesh_by_name(model, node->name);

		if (!mesh->user.d)
		{
			load3dsModelTextures(szFilename, model, mesh);
		}
	}
}

void load3dsModelTexturesIntoGl(const char* szFilename, Lib3dsFile* model)
{
	for (Lib3dsNode* p = model->nodes; p != NULL; p = p->next)
	{
		load3dsModelTextures(szFilename, model, p);
	}
}
