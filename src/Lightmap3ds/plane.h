#pragma once

// A geometric plane
struct Plane 
{
	float a, b, c, d;
};