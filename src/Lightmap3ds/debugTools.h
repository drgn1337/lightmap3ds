#pragma once

// Contains declarations for helpful debugging and logging functions

struct Lib3dsFile;
struct LightmapBakerSettings;

class LightmapPatch;
class BakedModel;

class DebugTools
{
public:
	static void Log(const char* format, ...);
	static void LogError(const char* format, ...);

public:
	static void Dump3dsInfo(const char* szFilename);

public:
	static void DumpBakedModel(const char* szFilename, const LightmapBakerSettings& settings);
	static void DumpBakedModel(BakedModel* model);
	static void DumpPatch(LightmapPatch* patch);
	static void DumpLumelsToDisk(const LightmapPatch* patch);
};
