#include "rectangle.h"

Rectangle::Rectangle()
{
	Zero();
}

Rectangle::Rectangle(float minx, float miny, float maxx, float maxy)
{
	min.x = minx;
	min.y = miny;
	min.z = 0;
	max.x = maxx;
	max.y = maxy;
	max.z = 0;
}

void Rectangle::Zero()
{
	min.Zero();
	max.Zero();
}

Vector Rectangle::GetCenter() const
{
	Vector center(
		(min.x + max.x) * 0.5f,
		(min.y + max.y) * 0.5f,
		0
	);
	return center;
}

Vector Rectangle::GetSize() const
{
	Vector size(
		max.x - min.x,
		max.y - min.y,
		0
	);
	return size;
}
