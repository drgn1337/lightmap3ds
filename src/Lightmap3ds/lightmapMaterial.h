#pragma once

#include "lightmapColor.h"

struct Lib3dsMaterial;

// Represents a material attached to faces of the model being baked. Multiple faces
// can share the same material.
struct LightmapMaterial
{
	// The face diffuse color
	LightmapColor diffuse;

	// The handle to a texture required by the 3ds model. This is not a handle
	// to a lumel map; that is maintained at the face level.
	int textureHandle;

	LightmapMaterial()
	{
		diffuse = LightmapColor(0, 0, 0);
		textureHandle = 0;
	}

	static LightmapMaterial From3dsMaterial(const Lib3dsMaterial* sourceMaterial);
};
