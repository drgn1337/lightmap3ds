#pragma once

#include <vector>
#include "box.h"
#include "vector.h"
#include "octreeFace.h"
#include "ray.h"

struct Lib3dsFile;
struct Lib3dsMesh;
struct Lib3dsNode;
struct Lib3dsFace;

class Octree
{
private:
	std::vector<OctreeFace*> faces;
	Box bounds;

private:
	std::vector<Octree*> children;
	std::vector<Octree*> XNegNeighbors;
	std::vector<Octree*> XPosNeighbors;
	std::vector<Octree*> YNegNeighbors;
	std::vector<Octree*> YPosNeighbors;
	std::vector<Octree*> ZNegNeighbors;
	std::vector<Octree*> ZPosNeighbors;
	Octree* parent;
	Octree* root;

private:
	void DeleteChildren();
	void DeleteFaces();

private:
	bool CanSubdivide() const;

public:
	Octree();
	~Octree();

private:
	void AddFaces(Lib3dsFile* file);
	void AddFaces(Lib3dsFile* file, Lib3dsNode* node);
	void CalculateBoundsFromFaces();
	void InheritFacesInsideOrIntersectingBounds(Octree& sourceTree, const Box& boundsToTest);

private:
	void Subdivide();

public:
	bool IsRoot() const;
	bool IsLeaf() const;
	Vector GetCenter() const;
	Vector GetSize() const;
	std::vector<Octree*> GetChildren() const;
	std::vector<Box> GetChildrenBounds() const;

public:
	const OctreeFace* Raycast(const Ray& ray) const;
	const Octree* GetLeafContainingPoint(const Vector& point) const;
	bool ContainsPoint(const Vector& point) const;
	bool GetBoundingBoxIntersectionPoint(const Ray& ray, Vector& intersection) const;
	const OctreeFace* GetFaceIntersectingRay(const Ray& ray) const;

public:
	static Octree* From3ds(struct Lib3dsFile* pFile);
};
