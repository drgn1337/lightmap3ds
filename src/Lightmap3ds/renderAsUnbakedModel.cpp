#include <lib3ds/file.h>
#include <lib3ds/node.h>
#include <lib3ds/mesh.h>
#include <lib3ds/matrix.h>
#include <lib3ds/material.h>
#include <lib3ds/light.h>
#include <GL/GLUT.H>
#include <CxImage/ximage.h>
#include <string>
#include <memory>
#include "bakedModel.h"
#include "renderAsUnbakedModel.h"

void renderNode(Lib3dsFile* file, Lib3dsNode* node)
{
	for (Lib3dsNode* child = node->childs; child != NULL; child = child->next)
	{
		renderNode(file, child);
	}

	if (node->type == LIB3DS_OBJECT_NODE) {
		Lib3dsMesh *mesh;

		if (strcmp(node->name, "$$$DUMMY") == 0) {
			return;
		}

		mesh = lib3ds_file_mesh_by_name(file, node->data.object.morph);
		if (mesh == NULL)
			mesh = lib3ds_file_mesh_by_name(file, node->name);

		if (!mesh->user.d) 
		{
			ASSERT(mesh);
			if (!mesh) {
				return;
			}
		}

		if (mesh->user.d) {
			Lib3dsObjectData *d;

			glPushMatrix();
			d = &node->data.object;
			glMultMatrixf(&node->matrix[0][0]);
			glTranslatef(-d->pivot[0], -d->pivot[1], -d->pivot[2]);
			glCallList(mesh->user.d);
			/* glutSolidSphere(50.0, 20,20); */
			glPopMatrix();
		}
		else
		{
			// Did not build a list
		}
	}
}

void light_update(Lib3dsFile* pFile, Lib3dsLight *l)
{
	Lib3dsNode *ln, *sn;

	ln = lib3ds_file_node_by_name(pFile, l->name, LIB3DS_LIGHT_NODE);
	sn = lib3ds_file_node_by_name(pFile, l->name, LIB3DS_SPOT_NODE);

	if (ln != NULL) {
		memcpy(l->color, ln->data.light.col, sizeof(Lib3dsRgb));
		memcpy(l->position, ln->data.light.pos, sizeof(Lib3dsVector));
	}

	if (sn != NULL)
		memcpy(l->spot, sn->data.spot.pos, sizeof(Lib3dsVector));
}

void updateLights(Lib3dsFile* pFile)
{
	if (NULL == pFile->lights)
	{
		glDisable(GL_LIGHTING);
	}
	else
	{
		glEnable(GL_LIGHTING);
		static const GLfloat a[] = { 0.0f, 0.0f, 0.0f, 1.0f };
		static GLfloat c[] = { 1.0f, 1.0f, 1.0f, 1.0f };
		static GLfloat p[] = { 0.0f, 0.0f, 0.0f, 1.0f };
		Lib3dsLight *l;

		int li = GL_LIGHT0;
		for (l = pFile->lights; l; l = l->next) {
			glEnable(li);

			light_update(pFile, l);

			c[0] = l->color[0];
			c[1] = l->color[1];
			c[2] = l->color[2];
			glLightfv(li, GL_AMBIENT, a);
			glLightfv(li, GL_DIFFUSE, c);
			glLightfv(li, GL_SPECULAR, c);

			p[0] = l->position[0];
			p[1] = l->position[2];
			p[2] = -l->position[1];
			glLightfv(li, GL_POSITION, p);

			if (l->spot_light) {
				p[0] = l->spot[0] - l->position[0];
				p[2] = l->spot[1] - l->position[1];
				p[1] = -(l->spot[2] - l->position[2]);
				glLightfv(li, GL_SPOT_DIRECTION, p);
			}
			++li;
		}
	}
}

void renderAsUnbakedModel(Lib3dsFile* model)
{
	updateLights(model);

	for (Lib3dsNode* p = model->nodes; p != NULL; p = p->next)
	{
		renderNode(model, p);
	}
}
