#include <GL/GLUT.H>
#include "renderBakedModel.h"
#include "bakedModel.h"
#include "lightmapPatch.h"
#include "lightmapFace.h"

int startIndex = 0;
int endIndex = 3;
int faceIndex = 0;

void renderBakedModel(BakedModel* model)
{
	glCallList((GLuint)model->GetUserData());
}
