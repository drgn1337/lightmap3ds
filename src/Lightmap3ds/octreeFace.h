#pragma once

#include "box.h"
#include "vector.h"
#include "ray.h"
#include "raycastHit.h"

class LightmapFace;

// Acts as a container for a lightmap face. This object is used to peform
// geometry tests in world space such as bounding box and raycast tests
class OctreeFace
{
public:
	enum BoxTestResult
	{
		OutOfBounds,
		Intersects,
		InBounds,
	};

private:
	// The face this object contains
	const LightmapFace* sourceFace;
	// The bounds containing this face in world coordinates
	Box bounds;

public:
	OctreeFace();

public:
	// Returns the lightmap face this object contains
	const LightmapFace* GetSourceFace() const;
	// Gets the bounding box that contains the face in world coordinates
	const Box& GetBounds() const;
	// Determines the spacial relationship between the bounds of this face and another box
	BoxTestResult BoxTest(const Box& box) const;
	// Determines whether a ray hits the contained face
	RaycastHit Raycast(const Ray& ray) const;

public:
	static OctreeFace* FromLightmapFace(const LightmapFace* lightmapFace);
};
