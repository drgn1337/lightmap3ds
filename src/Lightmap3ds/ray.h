#pragma once

#include "vector.h"

// Describes a point in world space and a vector protruding from that point
struct Ray
{
	Vector origin;
	Vector direction;

	Ray()
	{
	}

	Ray(const Vector& origin, const Vector& direction)
	{
		this->origin = origin;
		this->direction = direction;
	}
};
