#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "renderer.h"
#include "lightmapBakerSettings.h"
#include "commandLineTools.h"
#include "debugTools.h"

// Uncomment to hide the console window
//#pragma comment(linker, "/subsystem:\"windows\" /entry:\"mainCRTStartup\"")

void writeUsageToStderr()
{
	fputs("Renders a 3DS model file with baked lightmaps using OpenGL.\n", stderr);
	fputs("Usage: Lightmap3ds <filename> <switches>\n", stderr);
	fputs("\n", stderr);
	fputs("<Switches>\n", stderr);
	fputs("  -blur: Additionally blurs the generated lightmaps\n", stderr);
	fputs("  -dump3ds: Lists the contents of the 3ds file\n", stderr);
	fputs("  -dumpbakedmodel: Bakes the 3ds model, lists its contents and writes the lightmap bits to disk\n", stderr);
	fputs("  -forcesmoothing: Forces the 3ds model normals to be smoothed\n", stderr);
	fputs("  -quality: Set the multiplier for how much lumel space the faces use. The default value is 1\n", stderr);
	fputs("  -rendergrid: Renders a reference grid on the y=0 plane\n", stderr);
	fputs("  -renderunbaked: Renders the 3ds model using native OpenGL lighting\n", stderr);
}

int main(int argc, char *argv[])
{
	if (argc < 2)
	{
		writeUsageToStderr();
	}
	else if (containsArgument(argc, argv, "-dump3ds"))
	{
		DebugTools::Dump3dsInfo(argv[1]);
	}
	else if (containsArgument(argc, argv, "-dumpbakedmodel"))
	{
		DebugTools::DumpBakedModel(argv[1], LightmapBakerSettings::SettingsFromCommandLine(argc, argv));
	}
	else
	{
		// Programmer's note: This call is supposed to make rendererCleanup get called
		// when the program terminates, however it does not get called. Need to figure
		// out whether atexit is being used incorrectly or if there's another way to
		// ensure all allocated data is released from memory on termination
		atexit(rendererCleanup);

		rendererInit(argc, argv);
		rendererMainLoop();
	}
	return 0;
}
