#pragma once

// Contains declarations for rendering baked models

class BakedModel;

void renderBakedModel(BakedModel* model);
