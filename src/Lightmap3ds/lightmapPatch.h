#pragma once

#include "rectangle.h"

class LightmapFace;

// Represents an area in the lightmap that contains one or more faces. Each lightmap patch contains
// exactly one face as well as references to inner sub-patches which also contain exactly one face each
class LightmapPatch
{
public:
	enum AddFaceResult
	{
		Ok,
		FaceTooBig
	};

private:
	// The quality scale factor. The higher the number, the crisper the lightmap will appear.
	// A value of 1.0 means not to scale the UV's because their world coordinates are fine.
	float quality;

	// The position of the top-left corner of this patch in the texture
	int originX;
	int originY;

	// The size of the patch in texture pixels
	int width;
	int height;

	// The face that this patch contains and manages. Child patches manage other faces
	// that are contained within the bounds of this patch
	LightmapFace* managedFace;

	// The pointer to the memory allocated for the RGB values for every lumel in the map.
	// The root patch is responsible for allocation and and this value is the same for 
	// all patches within this one
	unsigned char* lumels;

	// The root patch
	LightmapPatch* root;

	// Child patches where other faces occupy the same space that
	// this patch contains. Children are arranged like so:
	//
	// +--------+----------+
	// |        |          |
	// |  this  | child[0] |
	// |        |          |
	// +--------+----------+
	// |                   |
	// |      child[1]     |
	// |                   |
	// +-------------------+
	//
	LightmapPatch* child[2];

	// User data
	void* userData;

private:
	LightmapPatch(int x, int y, int w, int h, float quality);
	~LightmapPatch();

public:
	// Makes this patch the root patch
	AddFaceResult TrySetFace(LightmapFace* face);

public:
	// Blurs the entire lightmap
	void Blur();
	
public:
	bool IsRootPatch() const;
	bool IsLeafPatch() const;

	int GetOriginX() const;
	int GetOriginY() const;
	int GetWidth() const;
	int GetHeight() const;

	LightmapFace* GetManagedFace() const;
	unsigned char* GetLumels() const;
	LightmapPatch* GetChild(int childIndex) const;

	int GetLightmapFaceTextureWidth(const LightmapFace* face) const;
	int GetLightmapFaceTextureHeight(const LightmapFace* face) const;

public:
	void SetUserData(void* data);
	void* GetUserData() const;

public:
	// Maximum size
	const static int TextureWidth = 1024;
	const static int TextureHeight = 1024;

	// Constant child count
	const static int ChildCount = 2;

	// The minimum number of lumels between the bounds of the face and the bounds of the texture
	const static int Padding = 2;

	// The number of bytes per texel
	const static int BytesPerLumel = 3;

public:
	static LightmapPatch* Create(float qualityScaleFactor);
	static void Destroy(LightmapPatch* patch);
};
