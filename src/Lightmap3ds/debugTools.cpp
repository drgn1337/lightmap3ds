#include <lib3ds\file.h>
#include <ctime>
#include "lightmapPatch.h"
#include "lightmapFace.h"
#include "lightmapVertex.h"
#include "bakedModel.h"
#include "rendererSetup.h"
#include "rendererSetupBakedModel.h"
#include "debugTools.h"
#include "CxImage\ximage.h"

void DebugTools::Log(const char* format, ...)
{
	time_t t = time(0);   // get time now
	struct tm * now = localtime(&t);
	char buffer[2048];
	va_list args;
	va_start(args, format);
	vsprintf_s(buffer, 2048, format, args);
	printf("[%02d:%02d:%02d] %s", now->tm_hour, now->tm_min, now->tm_sec, buffer);
	va_end(args);
}

void DebugTools::LogError(const char* format, ...)
{
	time_t t = time(0);   // get time now
	struct tm * now = localtime(&t);
	char buffer[2048];
	va_list args;
	va_start(args, format);
	vsprintf_s(buffer, 2048, format, args);
	fprintf(stderr, "[%02d:%02d:%02d] %s", now->tm_hour, now->tm_min, now->tm_sec, buffer);
	va_end(args);
}

void DebugTools::Dump3dsInfo(const char* szFilename)
{
	Lib3dsFile* file = lib3ds_file_load(szFilename);

	printf("Dumping materials:\n");
	lib3ds_file_dump_materials(file);
	printf("\n");

	printf("Dumping meshes:\n");
	lib3ds_file_dump_meshes(file);
	printf("\n");

	printf("Dumping instances:\n");
	lib3ds_file_dump_instances(file);
	printf("\n");

	printf("Dumping cameras:\n");
	lib3ds_file_dump_cameras(file);
	printf("\n");

	printf("Dumping lights:\n");
	lib3ds_file_dump_lights(file);
	printf("\n");

	printf("Dumping node hierarchy:\n");
	lib3ds_file_dump_nodes(file);
	printf("\n");

	lib3ds_file_free(file);
}

void DebugTools::DumpBakedModel(const char* szFilename, const LightmapBakerSettings& settings)
{
	Lib3dsFile* unbakedModel = load3dsModel(szFilename);
	BakedModel* model = bake3dsModel(unbakedModel, settings);
	DumpBakedModel(model);
	delete model;
}

void DebugTools::DumpBakedModel(BakedModel* model)
{
	printf("Dumping baked model patches:\n");
	for (auto lightmapPatch : model->GetLightmapPatches())
	{
		DumpPatch(lightmapPatch);
		DumpLumelsToDisk(lightmapPatch);
	}
}

void DebugTools::DumpPatch(LightmapPatch* patch)
{
	LightmapFace* managedFace = patch->GetManagedFace();
	if (nullptr != managedFace)
	{
		int originX = patch->GetOriginX();
		int originY = patch->GetOriginY();
		int width = patch->GetLightmapFaceTextureWidth(managedFace);
		int height = patch->GetLightmapFaceTextureHeight(managedFace);
		printf("  Patch (%d,%d,%d,%d)\n"
			, patch->GetOriginX()
			, patch->GetOriginY()
			, patch->GetOriginX() + patch->GetLightmapFaceTextureWidth(managedFace)
			, patch->GetOriginY() + patch->GetLightmapFaceTextureHeight(managedFace)
		);
		printf("  Vertices\n");

		const LightmapVertex *v = managedFace->GetVertices();
		for (int i = 0; i < LightmapFace::VertexCount; i++, v++)
		{
			printf("    World: %.02f %.02f %.02f  Proj: %.02f %.02f %.02f  UV: %.02f %.02f\n"
				, v->worldPos.x, v->worldPos.y, v->worldPos.z
				, v->projectedPos.x, v->projectedPos.y, v->projectedPos.z
				, v->lumelPos.u, v->lumelPos.v
			);
		}
	}

	for (int i = 0; i < LightmapPatch::ChildCount; i++)
	{
		if (nullptr != patch->GetChild(i))
		{
			DumpPatch(patch->GetChild(i));
		}
	}
}

void DebugTools::DumpLumelsToDisk(const LightmapPatch* patch)
{
	char szFilename[32];
	_snprintf_s(szFilename, 32, 32, "Lumels_0x%x.png", (unsigned int)patch);
	CxImage *image = new CxImage(patch->GetWidth(), patch->GetHeight(), LightmapPatch::BytesPerLumel * 8, CXIMAGE_FORMAT_PNG);
	memcpy(image->GetBits(), patch->GetLumels(), patch->GetWidth() * patch->GetHeight() * LightmapPatch::BytesPerLumel);
	image->Save(szFilename, CXIMAGE_FORMAT_PNG);
	delete image;
}
