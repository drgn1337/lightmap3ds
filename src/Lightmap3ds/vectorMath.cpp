#include <math.h>
#include "vectorMath.h"

void buildRotationMatrix(float* v, float fRad, float* m)
{
	float sinus = (float)sin(fRad);
	float cosinus = (float)cos(fRad);
	float V = 1 - cosinus;
	float a, b, c;

	a = v[0];
	b = v[1];
	c = v[2];

	m[0] = a*a*V + cosinus;
	m[4] = a*b*V - c*sinus;
	m[8] = a*c*V + b*sinus;
	m[12] = 0;

	m[1] = b*a*V + c*sinus;
	m[5] = b*b*V + cosinus;
	m[9] = b*c*V - a*sinus;
	m[13] = 0;

	m[2] = c*a*V - b*sinus;
	m[6] = c*b*V + a*sinus;
	m[10] = c*c*V + cosinus;
	m[14] = 0;

	m[3] = 0;
	m[7] = 0;
	m[11] = 0;
	m[15] = 1.0f;
}

void transformVector(float* v1, float* v2, float* matrix)
{
	float* m = matrix;
	float w;

	*v2 = (*v1) * (*m) + (*(v1 + 1)) * (*(m + 4)) + (*(v1 + 2)) * (*(m + 8)) + (*(m + 12));
	*(v2 + 1) = (*v1) * (*(m + 1)) + (*(v1 + 1)) * (*(m + 5)) + (*(v1 + 2)) * (*(m + 9)) + (*(m + 13));
	*(v2 + 2) = (*v1) * (*(m + 2)) + (*(v1 + 1)) * (*(m + 6)) + (*(v1 + 2)) * (*(m + 10)) + (*(m + 14));
	w = (*v1) * (*(m + 3)) + (*(v1 + 1)) * (*(m + 7)) + (*(v1 + 2)) * (*(m + 11)) + (*(m + 15));

	if (w != 0) {
		*v2 /= w; *(v2 + 1) /= w; *(v2 + 2) /= w;
	}
	else {
		*v2 = 0; *(v2 + 1) = 0; *(v2 + 2) = 0;
	}
}

float dotProduct(const Vector& v1, const Vector& v2)
{
	return v1.x * v2.x + v1.y * v2.y + v1.z * v2.z;
}
