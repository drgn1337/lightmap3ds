#include "box.h"
#include "minmax.h"
#include <lib3ds/mesh.h>

Box::Box()
{
	Zero();
}

Box::Box(float minx, float miny, float minz, float maxx, float maxy, float maxz)
{
	min.x = minx;
	min.y = miny;
	min.z = minz;
	max.x = maxx;
	max.y = maxy;
	max.z = maxz;
}

void Box::Zero()
{
	min.Zero();
	max.Zero();
}

void Box::Combine(const Box& other)
{
	min.x = min(min.x, other.min.x);
	min.y = min(min.y, other.min.y);
	min.z = min(min.z, other.min.z);
	max.x = max(max.x, other.max.x);
	max.y = max(max.y, other.max.y);
	max.z = max(max.z, other.max.z);
}

Vector Box::GetCenter() const
{
	Vector center(
		(min.x + max.x) * 0.5f,
		(min.y + max.y) * 0.5f,
		(min.z + max.z) * 0.5f
	);
	return center;
}

Vector Box::GetSize() const
{
	Vector size(
		max.x - min.x,
		max.y - min.y,
		max.z - min.z
	);
	return size;
}
