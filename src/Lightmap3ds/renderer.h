#pragma once

// Contains declarations for primary rendering functions

void rendererInit(int argc, char *argv[]);
void rendererMainLoop();
void renderScene();
void rendererCleanup();
