#pragma once

#include "vector.h"

// A geometric rectangle
class Rectangle
{
public:
	// Minimum extent
	Vector min;
	// Maximum extent
	Vector max;

public:
	Rectangle();
	Rectangle(float minx, float miny, float maxx, float maxy);

public:
	void Zero();

public:
	// Gets the center of the rectangle
	Vector GetCenter() const;
	// Gets the size of the rectangle
	Vector GetSize() const;
};
