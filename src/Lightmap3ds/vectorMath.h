#pragma once

// Contains vector math functions largely taken from legacy code in other projects

#include "vector.h"
#include "plane.h"

void buildRotationMatrix(float* v, float fRad, float* m);
void transformVector(float* v1, float* v2, float* matrix);
float dotProduct(const Vector& v1, const Vector& v2);
