#include <string.h>
#include "commandLineTools.h"

bool containsArgument(int argc, char* argv[], const char* arg)
{
	return getArgumentOrdinal(argc, argv, arg) >= 0;
}

int getArgumentOrdinal(int argc, char* argv[], const char* arg)
{
	for (int i = 0; i < argc; i++)
	{
		if (0 == strcmp(argv[i], arg))
		{
			return i;
		}
	}
	return -1;
}
