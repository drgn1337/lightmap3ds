#pragma once

#include "vector.h"
#include "lightmapColor.h"

struct Lib3dsLight;

// Represents a light source attached to the model being baked
struct LightmapLight
{
	enum LightType
	{
		// Omnidirectional (point) light
		Omni,
		// Spotlight
		Spot
	};

	// The type of light source
	LightType type;

	// The light color
	LightmapColor color;

	// The light source position
	Vector position;

	// The spotlight direction if this is a spotlight
	Vector spotDirection;

	// The cosine of the angle between the spotlight beam and the beam farthest away
	// from it such that everything in between is fully lit by the spotlight, and
	// everything outside loses illumination until the cutoff angle is reached
	float hotspotCosine;

	// The cosine of the angle between the spotlight beam and the beam farthest away
	// from it such that everything outside of it is unlit
	float cutoffCosine;

	static LightmapLight From3dsLight(const Lib3dsLight* sourceLight);
};
