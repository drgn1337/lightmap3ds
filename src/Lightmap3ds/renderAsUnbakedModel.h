#pragma once

// Contains declarations for rendering unbaked models

struct Lib3dsFile;

void renderAsUnbakedModel(Lib3dsFile* model);
