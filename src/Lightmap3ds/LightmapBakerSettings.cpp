#include "commandLineTools.h"
#include <stdlib.h>
#include "lightmapBakerSettings.h"

LightmapBakerSettings LightmapBakerSettings::SettingsFromCommandLine(int argc, char *argv[])
{
	LightmapBakerSettings settings;
	if (containsArgument(argc, argv, "-blur"))
	{
		settings.blur = true;
	}
	if (containsArgument(argc, argv, "-forcesmoothing"))
	{
		settings.forceSmoothing = true;
	}
	int qualityOrdinal = getArgumentOrdinal(argc, argv, "-quality");
	if (qualityOrdinal >= 0 && qualityOrdinal + 1 < argc)
	{
		settings.quality = (float)atof(argv[qualityOrdinal + 1]);
	}
	return settings;
}
