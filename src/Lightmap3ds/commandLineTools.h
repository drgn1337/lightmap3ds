#pragma once

// Contains declarations for helper functions used to extract program execution command-line parameters

bool containsArgument(int argc, char* argv[], const char* arg);
int getArgumentOrdinal(int argc, char* argv[], const char* arg);