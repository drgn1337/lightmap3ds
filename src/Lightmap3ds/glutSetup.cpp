#include <string.h>
#include <GL/GLUT.H>
#include "glutSetup.h"
#include "renderer.h"

bool needsDraw = false;

int windowWidth = 400;
int windowHeight = 400;

bool leftMouseButtonDown = false;
bool rightMouseButtonDown = false;
bool middleMouseButtonDown = false;

int mouseX = 0;
int mouseY = 0;

void beginRender()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void endRender()
{
	glutSwapBuffers();
}

void draw()
{
	beginRender();
	renderScene();
	endRender();
}

void OnForcedDraw()
{
	draw();
}

void OnDraw()
{
	if (needsDraw)
	{
		draw();
		needsDraw = false;
	}
}

void OnResize(int Width, int Height)
{
	glViewport(0, 0, Width, Height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f, 1.0f, 0.1f, 1000.0f);
	glMatrixMode(GL_MODELVIEW);

	windowWidth = Width;
	windowHeight = Height;

	needsDraw = true;
}

void OnMouseButton(int button, int state, int x, int y)
{
	if (button == GLUT_LEFT_BUTTON)
	{
		if (state == GLUT_DOWN)
		{
			leftMouseButtonDown = true;
			rightMouseButtonDown = false;
			middleMouseButtonDown = false;
		}
		else
		{
			leftMouseButtonDown = false;
		}
	}
	else if (button == GLUT_RIGHT_BUTTON)
	{
		if (state == GLUT_DOWN)
		{
			leftMouseButtonDown = false;
			rightMouseButtonDown = true;
			middleMouseButtonDown = false;
		}
		else
		{
			rightMouseButtonDown = 0;
		}
	}
	else if (button == GLUT_MIDDLE_BUTTON)
	{
		if (state == GLUT_DOWN)
		{
			leftMouseButtonDown = false;
			rightMouseButtonDown = false;
			middleMouseButtonDown = true;
		}
		else
		{
			middleMouseButtonDown = false;
		}
	}

	mouseX = x;
	mouseY = y;
	needsDraw = true;
}

void OnKeyboard(unsigned char key, int x, int y)
{
}

void OnMouseMotion(int x, int y)
{
	mouseX = x;
	mouseY = y;

	needsDraw = true;
}

const char *getFilename(const char *filename)
{
	const char *ptr = strrchr(filename, '/');
	if (NULL == ptr)
	{
		ptr = strrchr(filename, '\\');
	}
	return ptr != NULL ? ptr + 1 : filename;
}

void initializeGlutWindow(const char* szWindowName)
{
	glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH | GLUT_MULTISAMPLE);
	glutInitWindowSize(windowWidth, windowHeight);
	glutInitWindowPosition(0, 0);
	glutCreateWindow(szWindowName);
}

void setGlutCallbacks()
{
	glutDisplayFunc(&OnForcedDraw);
	glutIdleFunc(&OnDraw);
	glutReshapeFunc(&OnResize);
	glutMouseFunc(&OnMouseButton);
	glutKeyboardFunc(&OnKeyboard);
	glutMotionFunc(&OnMouseMotion);
}

void initializeGlutScene()
{
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glClearDepth(1.0);
	glDepthFunc(GL_LESS);
	glEnable(GL_DEPTH_TEST);
	glShadeModel(GL_SMOOTH);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f, 1.0f, 0.1f, 1000.0f);

	glMatrixMode(GL_MODELVIEW);
	glEnableClientState(GL_VERTEX_ARRAY);
	glEnable(GL_MULTISAMPLE_ARB);
}

void initializeGlutSimulation(int argc, char *argv[])
{
	glutInit(&argc, argv);
	initializeGlutWindow(getFilename(argv[1]));
	setGlutCallbacks();
	initializeGlutScene();
}

bool isLeftMouseButtonDown()
{
	return leftMouseButtonDown;
}

bool isRightMouseButtonDown()
{
	return rightMouseButtonDown;
}

bool isMiddleMouseButtonDown()
{
	return middleMouseButtonDown;
}

int getMouseX()
{
	return mouseX;
}

int getMouseY()
{
	return mouseY;
}

int getWindowWidth()
{
	return windowWidth;
}

int getWindowHeight()
{
	return windowHeight;
}
