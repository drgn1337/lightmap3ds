#include <lib3ds\file.h>
#include <lib3ds\mesh.h>
#include <lib3ds\matrix.h>
#include <lib3ds\material.h>
#include <lib3ds\node.h>
#include <GL\GLUT.H>
#include <memory>
#include "rendererSetupUnbakedModel.h"

void generateGlListsFrom3dsMesh(Lib3dsFile* model, Lib3dsMesh* mesh)
{
	int tex_mode = 0; // Texturing active ? 
	mesh->user.d = glGenLists(1);
	glNewList(mesh->user.d, GL_COMPILE);

	{
		unsigned p;
		Lib3dsVector *normalL = (Lib3dsVector*)malloc(3 * sizeof(Lib3dsVector)*mesh->faces);
		Lib3dsMaterial *oldmat = (Lib3dsMaterial *)-1;
		{
			Lib3dsMatrix M;
			lib3ds_matrix_copy(M, mesh->matrix);
			lib3ds_matrix_inv(M);
			glMultMatrixf(&M[0][0]);
		}
		lib3ds_mesh_calculate_normals(mesh, normalL);

		for (p = 0; p<mesh->faces; ++p) {
			Lib3dsFace *f = &mesh->faceL[p];
			Lib3dsMaterial *mat = 0;
			if (f->material[0]) {
				mat = lib3ds_file_material_by_name(model, f->material);
			}

			if (mat != oldmat)
			{
				if (mat)
				{
					if (mat->two_sided)
						glDisable(GL_CULL_FACE);
					else
						glEnable(GL_CULL_FACE);

					glDisable(GL_CULL_FACE);

					if (mat->texture1_map.name[0])
					{
						tex_mode = (nullptr != mat->texture1_map.user.p);
						float black[3] = { 0,0,0 };
						float white[3] = { 1,1,1 };
						glMaterialfv(GL_FRONT, GL_AMBIENT, black);
						glMaterialfv(GL_FRONT, GL_DIFFUSE, white);
						glMaterialfv(GL_FRONT, GL_SPECULAR, black);
					}
					else
					{
						tex_mode = 0; // No texture for this material
						glMaterialfv(GL_FRONT, GL_AMBIENT, mat->ambient);
						glMaterialfv(GL_FRONT, GL_DIFFUSE, mat->diffuse);
						glMaterialfv(GL_FRONT, GL_SPECULAR, mat->specular);
						glMaterialf(GL_FRONT, GL_SHININESS, pow(2, 10.0*mat->shininess));
					}
				}
				else {
					static const Lib3dsRgba a = { 0.7f, 0.7f, 0.7f, 1.0f };
					static const Lib3dsRgba d = { 0.7f, 0.7f, 0.7f, 1.0f };
					static const Lib3dsRgba s = { 1.0f, 1.0f, 1.0f, 1.0f };
					glMaterialfv(GL_FRONT, GL_AMBIENT, a);
					glMaterialfv(GL_FRONT, GL_DIFFUSE, d);
					glMaterialfv(GL_FRONT, GL_SPECULAR, s);
					glMaterialf(GL_FRONT, GL_SHININESS, pow(2, 10.0*0.5));
				}
				oldmat = mat;
			}
			else if (mat != NULL && mat->texture1_map.name[0])
			{
				tex_mode = (nullptr != mat->texture1_map.user.p);
			}

			{
				int i;

				if (tex_mode)
				{
					glEnable(GL_TEXTURE_2D);
					glBindTexture(GL_TEXTURE_2D, (GLuint)mat->texture1_map.user.p);
				}

				glBegin(GL_TRIANGLES);
				glNormal3fv(f->normal);
				for (i = 0; i<3; ++i) {
					glNormal3f(
						normalL[3 * p + i][0],
						normalL[3 * p + i][2],
						-normalL[3 * p + i][1]
						);

					if (tex_mode) 
					{
						glTexCoord2f(mesh->texelL[f->points[i]][0], mesh->texelL[f->points[i]][1]);
					}

					glVertex3f(
						mesh->pointL[f->points[i]].pos[0],
						mesh->pointL[f->points[i]].pos[2],
						-mesh->pointL[f->points[i]].pos[1]
					);
				}
				glEnd();

				if (tex_mode)
					glDisable(GL_TEXTURE_2D);
			}
		}

		free(normalL);
	}

	glEndList();
}

void generateGlListsFrom3dsNode(Lib3dsFile* model, Lib3dsNode* node)
{
	for (Lib3dsNode* child = node->childs; child != NULL; child = child->next)
	{
		generateGlListsFrom3dsNode(model, child);
	}

	if (node->type == LIB3DS_OBJECT_NODE)
	{
		Lib3dsMesh *mesh;

		if (strcmp(node->name, "$$$DUMMY") == 0) {
			return;
		}

		mesh = lib3ds_file_mesh_by_name(model, node->data.object.morph);
		if (mesh == NULL)
			mesh = lib3ds_file_mesh_by_name(model, node->name);

		if (!mesh->user.d)
		{
			generateGlListsFrom3dsMesh(model, mesh);
		}
	}
}

void generateGlListsFrom3dsModel(Lib3dsFile* model)
{
	for (Lib3dsNode* p = model->nodes; p != NULL; p = p->next)
	{
		generateGlListsFrom3dsNode(model, p);
	}
}
