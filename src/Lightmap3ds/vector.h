#pragma once

#include <math.h>

// Describes a coordinate in world space
struct Vector
{
	float x, y, z;

	Vector()
	{
		Zero();
	}

	Vector(const Vector& source)
	{
		x = source.x;
		y = source.y;
		z = source.z;
	}

	Vector(float x, float y, float z)
	{
		this->x = x;
		this->y = y;
		this->z = z;
	}

	Vector(const float source[3])
	{
		x = source[0];
		y = source[1];
		z = source[2];
	}

	void Zero()
	{
		x = y = z = 0;
	}

	float Magnitude() const
	{
		return sqrtf(x*x + y*y + z*z);
	}

	Vector Normalized() const
	{
		float magnitude = Magnitude();
		if (magnitude < 0.0001f)
		{
			return Vector();
		}
		else
		{
			Vector normalized{
				x / magnitude,
				y / magnitude,
				z / magnitude
			};
			return normalized;
		}
	}

	Vector operator-()
	{
		return Vector(-x, -y, -z);
	}

	static Vector From3dsVector(const float source[3])
	{
		return Vector(source[0], source[2], -source[1]);
	}
};

inline Vector operator +(const Vector& v1, const Vector& v2)
{
	Vector v;
	v.x = v1.x + v2.x;
	v.y = v1.y + v2.y;
	v.z = v1.z + v2.z;
	return v;
}

inline Vector operator -(const Vector& v1, const Vector& v2)
{
	Vector v;
	v.x = v1.x - v2.x;
	v.y = v1.y - v2.y;
	v.z = v1.z - v2.z;
	return v;
}

inline Vector operator *(const Vector& v1, const float s2)
{
	Vector v;
	v.x = v1.x * s2;
	v.y = v1.y * s2;
	v.z = v1.z * s2;
	return v;
}

inline Vector operator *(const Vector& v1, const Vector& v2)
{
	Vector vc;
	vc.x = v1.y*v2.z - v1.z*v2.y;
	vc.y = v1.z*v2.x - v1.x*v2.z;
	vc.z = v1.x*v2.y - v1.y*v2.x;
	return vc;
}

