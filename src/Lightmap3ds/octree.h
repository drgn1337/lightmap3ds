#pragma once

#include <vector>
#include "box.h"
#include "vector.h"
#include "octreeFace.h"
#include "ray.h"
#include "raycastSettings.h"
#include "raycastHit.h"

class LightmapPatch;
class LightmapFace;
class BakedModel;

// A node in a geometric box which may contain eight equally divided sub-nodes. Octrees
// are used to partition model faces by proximity in a way that provides for efficent
// traversal of casted rays.
class Octree
{
private:
	// The collection of lightmap face containers for faces that intersect or are contained within this node
	std::vector<OctreeFace*> faces;
	// The bounding box fo the node
	Box bounds;

private:
	// A pointer to this node's parent node
	Octree* parent;
	// A pointer to the node that has no parent
	Octree* root;
	// A collection of pointers that make up this node's children
	Octree** children;
	// The node that neighbors this one in the X+ direction
	Octree* neighborXPos;
	// The node that neighbors this one in the X- direction
	Octree* neighborXNeg;
	// The node that neighbors this one in the Y+ direction
	Octree* neighborYPos;
	// The node that neighbors this one in the Y- direction
	Octree* neighborYNeg;
	// The node that neighbors this one in the Z+ direction
	Octree* neighborZPos;
	// The node that neighbors this one in the Z- direction
	Octree* neighborZNeg;

private:
	// The depth of this node. The root's depth is 0 and the root's immediate
	// children depths is 1.
	int depth;

private:
	// Deletes all child nodes
	void DeleteChildren();
	// Deletes all octree faces but leaves the inner LightmapFace preserved in memory
	void DeleteFaces();

private:
	// True if this node can be broken down further
	bool CanSubdivide() const;

private:
	Octree();
	Octree(Octree* parent, const Box& bounds);
public:
	~Octree();

private:
	// Assigns the neighbors of this node
	void SetNeighbors(Octree* xpos, Octree* xneg, Octree* ypos, Octree* yneg, Octree* zpos, Octree* zneg);
	// Adds faces to this tree
	void AddFaces(const std::vector<LightmapFace*> lightmapFaces);
	void AddFace(const LightmapFace* lightmapFace);
	// Calculates the bounds for this node
	void CalculateBoundsFromFaces();
	// Tries to inherit faces from this node's parent
	void InheritFacesInsideOrIntersectingBoundsFromParent();

private:
	// Allocate children for this node and assign faces to them
	void Subdivide();

public:
	// True if this is the root node
	bool IsRoot() const;
	// True if this node has no children
	bool IsLeaf() const;
	// Gets the center of this node in world space
	Vector GetCenter() const;
	// Gets the size of this node in world space
	Vector GetSize() const;
	// Gets the children of this node
	Octree** GetChildren() const;

public:
	// Determines if a ray hits a face in a child node. This should only be called on the root node.
	RaycastHit Raycast(const RaycastSettings& settings) const;
	// Gets the leaf node containing a point in world space
	const Octree* GetLeafContainingPoint(const Vector& point) const;
	// True if this node contains a point in world space
	bool ContainsPoint(const Vector& point) const;
	// Gets the point where a ray intersects this node
	bool GetBoundingBoxIntersectionPoint(const Ray& ray, Vector& intersection) const;

public:
	static Octree* FromLightmapFaces(std::vector<LightmapFace*> lightmapFaces);

private:
	const int MaxChildren = 8;
};
