#pragma once

#include <math.h>

// Describes a coordinate in texture space. All axe values are in the range [0,1]
struct Texel
{
	float u, v;

	Texel()
	{
		Zero();
	}

	Texel(float u, float v)
	{
		this->u = u;
		this->v = v;
	}

	void Zero()
	{
		u = v = 0;
	}

	float Magnitude() const
	{
		return sqrtf(u*u + v*v);
	}

	Texel Normalized() const
	{
		float magnitude = Magnitude();
		if (magnitude < 0.0001f)
		{
			return Texel();
		}
		else
		{
			Texel normalized{
				u / magnitude,
				v / magnitude,
			};
			return normalized;
		}
	}

	static Texel From3dsTexel(const float source[2])
	{
		return Texel(source[0], source[1]);
	}
};

inline Texel operator +(const Texel& t1, const Texel& t2)
{
	Texel t;
	t.u = t1.u + t2.u;
	t.v = t1.v + t2.v;
	return t;
}

inline Texel operator -(const Texel& t1, const Texel& t2)
{
	Texel t;
	t.u = t1.u - t2.u;
	t.v = t1.v - t2.v;
	return t;
}

inline Texel operator *(const Texel& t1, const float s2)
{
	Texel t;
	t.u = t1.u * s2;
	t.v = t1.v * s2;
	return t;
}

inline Texel operator /(const Texel& t1, const float s2)
{
	Texel t;
	t.u = t1.u / s2;
	t.v = t1.v / s2;
	return t;
}
