#pragma once

#include "vector.h"
#include "texel.h"

// Represents a single vertex in a LightmapFace
struct LightmapVertex
{
	// The position of the vertex in world space
	Vector worldPos;

	// The position of the vertex in world space projected onto the lightmap face's favored axis
	Vector projectedPos;

	// The position of the vertex on the model's texture map. Each unit is in the range [0,1]
	Texel texelPos;

	// The position of the vertex on the light map. Each unit is in the range [0,1]
	Texel lumelPos;

	// The normal of the face containing the vertex
	Vector normal;
};
