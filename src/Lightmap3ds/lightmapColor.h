#pragma once

#include "minmax.h"

// Describes an RGB color
struct LightmapColor
{
	float r, g, b;

	LightmapColor()
	{
		Zero();
	}

	LightmapColor(float red, float green, float blue)
	{
		r = red;
		g = green;
		b = blue;
	}

	LightmapColor(const float source[3])
	{
		r = source[0];
		g = source[1];
		b = source[2];
	}

	void Zero()
	{
		r = g = b = 0;
	}

	void Clamp()
	{
		r = min(1, max(0, r));
		g = min(1, max(0, g));
		b = min(1, max(0, b));
	}

	inline LightmapColor& LightmapColor::operator+=(const LightmapColor& c)
	{
		this->r += c.r;
		this->g += c.g;
		this->b += c.b;
		return *this;
	}

	inline LightmapColor& LightmapColor::operator*=(float intensity)
	{
		this->r *= intensity;
		this->g *= intensity;
		this->b *= intensity;
		return *this;
	}

	static unsigned char FloatToLumelByte(float l)
	{
		return (unsigned char)( min(1.0f, max(0.0f, l)) * 255.0f );
	}
};

inline LightmapColor operator +(const LightmapColor& c1, const LightmapColor& c2)
{
	LightmapColor c;
	c.r = c1.r + c2.r;
	c.g = c1.g + c2.g;
	c.b = c1.b + c2.b;
	return c;
}

inline LightmapColor operator *(const LightmapColor& c1, const LightmapColor& c2)
{
	LightmapColor c;
	c.r = c1.r * c2.r;
	c.g = c1.g * c2.g;
	c.b = c1.b * c2.b;
	return c;
}

inline LightmapColor operator *(const LightmapColor& source, float intensity)
{
	LightmapColor c;
	c.r = source.r * intensity;
	c.g = source.g * intensity;
	c.b = source.b * intensity;
	return c;
}
